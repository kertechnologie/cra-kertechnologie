import React from 'react';
import '../App.css';
import axios from 'axios';
import { withRouter } from 'react-router';
import logoKertech from '../logoKertech.json';
import { parseJwt } from '../util/JWParser';
import {Bar} from 'react-chartjs-2';
import conges from './conges'
import jsPDF from 'jspdf';
import "jspdf-autotable";
import NavigationBar from './NavigationVar';
import cra_data from '../data/CRA_data.json'
import { Redirect } from 'react-router-dom';
import logo from '../logo.png';



class facture extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        option_month:[],
        option_year:[],
        affectation:[],
        selectedMonth:'',
        selectedYear:'',
        current_month:'',
        Month_converted_front:[],
        dayOfWork:[],
        weekdays:[],
        dayAbsent:[],
        nb_tache:[],
        dtr:'',
        consultant:[],
        cliat : [],
        infoTable:'',
        Pourcentage:'',
        style:'',
        inputValue: '',
        month:'',
        quantite:'',
        year:'',
      factures:[],
      clients:[],
      factureById:{},
      index:null,
      abcdF:'',
      consults:[],
      egl:'',
      users:[],
      firstname:'',
      tvtau:'',
      query: "",
      data: [],
      som:'',
      blockch:[],
      filteredData: [],
      dv : []  
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeYear=this.handleChangeYear.bind(this);
    this.handleChangeMonth=this.handleChangeMonth.bind(this);
    this.generatePDF=this.generatePDF.bind(this);
    this.updateSelectValue=this.updateSelectValue.bind(this);
    this.updateInputValue=this.updateInputValue.bind(this);
    this.handleChngs = this.handleChngs.bind(this);
  }

  handleChange(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case 'client': {
            this.state.factureById.client = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;
          case 'num_facture': {
            this.state.factureById.num_facture = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break; 
          case 'date_facture': {
            this.state.factureById.date_facture = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;  
          case 'message': {
            this.state.factureById.message = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;   
          case 'num_engagement': {
            this.state.factureById.num_engagement = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;
          case 'code_service': {
            this.state.factureById.code_service = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;
          case 'condition_paiement': {
            this.state.factureById.condition_paiement = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;
          case 'echeance': {
            this.state.factureById.echeance = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;
          case 'consultant': {
            this.state.factureById.consultant = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;
          case 'annee': {
            this.state.factureById.annee = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break; 
          case 'mois': {
            this.state.factureById.mois = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break; 
          case 'nbr_jours': {
            this.state.factureById.nbr_jours = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break; 
          case 'unite': {
            this.state.factureById.unite = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break; 
          case 'tjm': {
            this.state.factureById.tjm = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break; 
          case 'taux_tva': {
            this.state.factureById.taux_tva = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break; 
          case 'montant': {
            this.state.factureById.montant = e.target.value;
            this.setState({ factureById: this.state.factureById });
          }
          break;  
          default: 
          break;
        }   
      }
    }
  }

  handleChngs(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let blockch = [...this.state.blockch];
            blockch[i] = e.target.value;
            this.setState({  blockch  });
          }
        break;  
        default:
        break;
        }
      }
    }
  }

  createUI(){
    return this.state.blockch?.map((el, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre nom du client" value={el||''} class="form-control" onChange={this.handleChngs.bind(this, i)} />       
        <button value='remove' class="detr" onClick={this.removeClick.bind(this, i)}>
        <i class="fa fa-minus sam"></i></button>  


        <div>
                    <div class="lines"></div>
                    <div class="col-md-12 alin ratp" onLoad={this.OptionsSelect()}>
                    <div class="form-group col-md-3">
                    <label for="exampleFormControlSelect1" class="qimzq">Prestation</label>
                      <input type="text" value={el||''} class="form-control" onChange={this.handleChngs.bind(this, i)} placeholder="insérer le consultant"></input>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">  Veuillez selectionner une année </label>
                      <select className="form-control" value={el||''} onChange={this.handleChngs.bind(this, i)}>
                            <option  value=""></option>
                            {this?.state.option_year}
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                       <label for="exampleFormControlSelect1" class="labla qimzq"> Veuillez selectionner un mois</label>
                       <select className="form-control" value={el||''} onChange={this.handleChngs.bind(this, i)}>
                            <option  value=""></option>
                            {this?.state.option_month}
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Quantité</label>
                      <input type="text" class="form-control" value={this.state.quantite} onChange={eqt => this.updateQantiteValue(eqt)}/>
                    </div>
                    </div>
                    <div class="lines"></div>
                    <div class="col-md-12 alin sncf">
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Unité</label>
                      <select required class="form-control" value={this.state.factureById.unite} onChange={this.handleChange.bind(this, "unite")}>
                        <option value=""></option>
                        <option value="h">heure</option>
                        <option value="jour">jours</option>
                      </select>                
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">TJM</label>
                      <input type="text" class="form-control" value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} placeholder="000.00" />
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Taux de TVA</label>
                      <select required class="form-control" value={this.state.tvtau} onChange={eat => this.updateSelectValue(eat)}>
                        <option value="0%">0%</option>
                        <option value="2%">2%</option>
                        <option value="5%">5%</option>
                        <option value="10%">10%</option>
                        <option value="20%">20%</option>
                      </select>  
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Montant</label>
                      <input  type="text" class="form-control" value={this.state.som} ></input>
                    </div>
                    </div>
                    </div>


      </div>   
    )
  }

  addClick(){
    this.setState(prevState => ({ blockch: [...prevState.blockch, '']}))
    console.log("cc", this.state.blockch)
  }
  
  removeClick(i){
     let blockch = [...this.state.blockch];
     blockch.splice(i,1);
     this.setState({ blockch });
  }


  updateQantiteValue(eqt){
    this.setState({
      quantite: eqt.target.value
    });
  }

  updateInputValue(evt) {
    this.setState({
      inputValue: evt.target.value
    });
    var num = parseInt(this.state.quantite)
    var numb = parseInt(this.state.inputValue)
    this.state.som = num * numb

  }


  updateSelectValue(eat) {
    this.state.tvtau= eat.target.value
    this.setState({
      tvtau: this.state.tvtau
    });
    var num = parseInt(this.state.quantite)
        var numb = parseInt(this.state.inputValue)
        var dhar = num * numb
    if(this.state?.tvtau == "0%"){
      this.state.egl = 0
        this.state.som = dhar
    }
    else if(this.state?.tvtau == "2%"){
      this.state.egl = (dhar*2)/100
        this.state.som = dhar + ((dhar*2)/100)
    }else if(this.state?.tvtau == "5%"){
      this.state.egl = (dhar*5)/100
        this.state.som = dhar + ((dhar*5)/100)
    }else if(this.state?.tvtau == "10%"){
      this.state.egl = (dhar*10)/100
        this.state.som = dhar + ((dhar*10)/100)
    }else if(this.state?.tvtau == "20%"){
      this.state.egl = (dhar*20)/100
        this.state.som = dhar + ((dhar*20)/100)
    }else{
        this.state.som = dhar
    }
    console.log(this.state.egl)
  }

  componentDidMount() {    
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        for(var i=0; i<data.length; i++){
          this.setState({users:data})   
        //  this.state.dv = this.state.users[i].nom
        }
      })   
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllClients")
      .then(response => response.data)
      .then((data) => {
        this.setState({ clients: data });  
      })
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllFactures")
      .then(response => response.data)
      .then((data) => {
        this.setState({ factures: data });  
      });
  }



  OptionsSelect() {        
    var current_month_index = new Date().getMonth();
    for (var i = 0; i < 100; i++) {
        if (i < 12) {
            this.state.month=i;
            this.state.option_month.push(
                <option value={this.state.month}>{cra_data[i].month}</option>
            )
        }
        this.state.option_year.push(<option slected='true' value={parseInt(cra_data[i].year) + i}>{parseInt(cra_data[i].year) + i}</option>)
    }
//this.setState({option_month:this.state.option_month})
}

componentWillMount() {
    this.componentDidMount();
  }
  

  AddFacture(){
    this.state.factureById.client = '';
    this.state.factureById.num_facture = '';
    this.state.factureById.date_facture = '';
    this.state.factureById.message = '';
    this.state.factureById.num_engagement = '';
    this.state.factureById.code_service = '';
    this.state.factureById.condition_paiement = '';
    this.state.factureById.echeance = '';
    this.state.factureById.consultant = '';
    this.state.factureById.annee = '';
    this.state.factureById.mois = '';
    this.state.factureById.nbr_jours = '';
    this.state.factureById.unite = '';
    this.state.factureById.tjm = '';
    this.state.factureById.taux_tva = '';
    this.state.factureById.montant = '';
    this.setState({ factureById: this.state.factureById })
  } 

  //Ajouter un facture
  ValidateFacture() { 
      this.state.factureById.annee = this.selectedYear
      this.state.factureById.mois = this.state.selectedMonth
      this.state.factureById.nbr_jours = this.state.quantite
      this.state.factureById.montant = this.state.som
      this.state.factureById.taux_tva = this.state.tvtau
      this.state.factureById.tjm = this.state.inputValue
      console.log(this.state.factureById.mois)
      console.log(this.state.selectedMonth)

    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/aFacture", this.state.factureById) 
      .then(response => response.data)
      .then((factureById) => {
        this.state.factures.push(factureById);  
        this.setState({factures:this.state.factures});
      //  window.location.reload(false);

      }).catch((error) => { console.log(error) 
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        un probléme detetcté lors de l'insertion de votre nouveau facture</p>
        this.setState({message:this.state.message})
      });
  }

  //supprimer un facture
  DeleteFacture(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/deleteFacture/"+this.state.factureById.idfacture)
      .then(response => response.data)
      .then((data) => {
        this.state.factures.splice(this.state.index,1);
        this.setState({users:this.state.factures});
      //  window.location.reload(false);
      }).catch((error) => { console.log(error) });
  }

  //lire un facture (pas tout les factures)
  handleSort(idfacture,index) {
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/facture/" + idfacture)
      .then(response => response.data)
      .then((data) => {
        this.state.inputValue = data.tjm
        this.state.tvtau = data.taux_tva
        this.state.som=data.montant
        this.state.quantite = data.nbr_jours
        this.state.selectedMonth = data.mois
        this.state.selectedYear = data.annee
        var num = parseInt(this.state.quantite)
        var numb = parseInt(this.state.inputValue)
        var dhar = num * numb
         this.state.abcdF = num * numb
        console.log(this.state.abcdF)
        if(this.state.tvtau == "0%"){
          this.state.egl = 0
        }
        else if(this.state.tvtau == "2%"){
          this.state.egl = (dhar*2)/100
        }else if(this.state.tvtau == "5%"){
          this.state.egl = (dhar*5)/100
        }else if(this.state.tvtau == "10%"){
          this.state.egl = (dhar*10)/100
        }else if(this.state.tvtau == "20%"){
          this.state.egl = (dhar*20)/100
        }
    //    var abcd = this.state.egl.toString();
    
//        var ttr = this.state.factureById.tjm.split(".")
        this.setState({ factureById: data });

        this.state.index=index ;    
       
    
      });
  }

  //modifier un facture
  UpdateFacture(){
    
      this.state.factureById.annee = this.selectedYear
      this.state.factureById.mois = this.state.selectedMonth
      this.state.factureById.nbr_jours = this.state.quantite
      this.state.factureById.montant = this.state.som
      this.state.factureById.taux_tva = this.state.tvtau
      this.state.factureById.tjm = this.state.inputValue
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/updateFacture",this.state.factureById)
      .then(response => response.data)
      .then((data) => {
        this.state.factures.splice(this.state.index,1);
        this.state.factures.push(data);
        this.setState({factures:this.state.factures});
     //   window.location.reload(false);
      }).catch((error) => { console.log(error)
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        votre modification est echouée</p>
        this.setState({message:this.state.message}) 
      });
  }


handleChangeUser(event){
    // console.log(event.target.value)  
//    this.OptionsSelect()
this.setState({selectedMonth:''})
    this.state.Pourcentage='';
    this.state.month='';
    this.setState({pr:this.state.Pourcentage})
    var today=new Date();
    var i = 0;
    var format_date= this.formatDate(today);
   // var wda=this.WorkingDays(this.state.selectedMonth,2019)
    this.state.developper = event.target.value
    this.setState({developper:this.state.developper});

    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user/"+this.state.developper)
    .then(response => response.data)
    .then((data) => {
            this.setState({consultant:data})
            console.log("a",this.state.consultant.nom)
            for(var i=0; i<this.state.clients.length; i++){
                if(this.state.clients[i].consultant.includes(this.state.consultant.nom)){
                   console.log("tst", this.state.clients[i].societe)
                   console.log("tst", this.state.clients[i])
                }
            }
    } 
    )
  /*  axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllClients")
    .then(response => response.data)
    .then((data) => {
            this.setState({clients:data})
            console.log("et", this.state.cliat)
    } 
    )*/
    axios.get("http://45.9.188.225:8080/cra/Tache/"+this.state.developper)
    .then(response => response.data)
    .then((data) => {                      
        this.setState({affectation:data})
        this.state.affectation.forEach(tasks => {
           var  month =this.convert_to_front(format_date,tasks.date_tache).getMonth(); 
            if (tasks.affectation && this.state.Month_converted_front.indexOf(month)==-1){
                this.state.Month_converted_front.push(this.convert_to_front(format_date,tasks.date_tache).getMonth());} 
                //this.state.dayOfWork.push(this.formatDate(this.convert_to_front(format_date,tasks.date_tache)))
                i++;
                //console.log(tasks)
                })
                this.setState({dayOfWork:i})
                console.log("ssssssss", this.state.dayOfWork)
                                })

}

consultFacture(idfacture, index){
  axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
  axios.get("http://45.9.188.225:8080/cra/AllClients")
    .then(response => response.data)
    .then((data) => {
      this.setState({ clients: data });  
    })
  axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/facture/" + idfacture)
      .then(response => response.data)
      .then((data) => {
        this.setState({ factureById: data });
        this.state.index=index ;         
    for(var i=0; i<this.state.clients.length; i++){
      if(this.state.factureById.client == this.state.clients[i].societe){
        console.log("adresse",this.state.clients[i].adresse_postale_societe)

        
        var num = parseInt(this.state.factureById.nbr_jours)
        var numb = parseInt(this.state.factureById.tjm)
        var dhar = num * numb
        console.log(this.state.dhar)
    if(this.state.factureById.taux_tva == "0%"){
      this.state.egl = 0
    }
    else if(this.state.factureById.taux_tva == "2%"){
      this.state.egl = (dhar*2)/100
    }else if(this.state.factureById.taux_tva == "5%"){
      this.state.egl = (dhar*5)/100
    }else if(this.state.factureById.taux_tva == "10%"){
      this.state.egl = (dhar*10)/100
    }else if(this.state.factureById.taux_tva == "20%"){
      this.state.egl = (dhar*20)/100
    }
    console.log(this.state.egl)
  var abcd = this.state.egl.toString();

  var ttr = this.state.factureById.tjm.split(".")
  console.log(ttr)
    const doc = new jsPDF();
   
    const tableColumn = [ "Consultant", "Période","Quantité","Unité", "TJM","TVA", "Montant"];
    const tableRows = [];
  //  const erta = [this.state.factureById.tjm];
      //this.state.factureById.forEach(ticket => {
     var dto = this.state.factureById.mois +' ' + this.state.factureById.annee;
      const ticketData = [
      //  this.state.factureById.id,
      this.state.factureById.consultant,
      dto,
      this.state.factureById.nbr_jours,
      this.state.factureById.unite,
      this.state.factureById.tjm,
      this.state.factureById.taux_tva,
      this.state.factureById.montant
      ];
      tableRows.push(ticketData);
   // });
  
    doc.autoTable(tableColumn, tableRows, { startY: 135 });
    //const date = Date().split(" ");
   // const dateStr = date[0] + date[1] + date[2] + date[3] + date[4] + date[5] + date[6];
    
    doc.addImage(logoKertech.base64, 'JPG', 11, 10, 27, 27)
    doc.setFontSize(10)
   // doc.text(20,160,10,"test + ");
    doc.text(150, 170, "TOTAL HT")
    doc.text(180, 170, dhar+"$")
    doc.text(150, 175, "TVA ("+this.state.factureById.taux_tva+")")
    doc.text( abcd+"$",182, 175)
  
    doc.text(this.state.factureById.message , 15, 125);
    doc.text(15, 45, "Kertechnologie")
    doc.text(15, 50, "Khaled ERJILI")
    doc.text(15, 55, "264 Avenue Victor Hugo")
    doc.text(15, 60, "94120 Fontenay-sous-Bois")
    doc.text(15, 65, "0123456789")
    doc.text(15, 70, "contact@kertechnologie.fr")
    doc.text(15, 75, "www.kertechnologie.fr")
    doc.text(130, 20, "Date de Facturation :")
    doc.text(165, 20, this.state.factureById.date_facture)
    doc.text(120, 50, this.state.clients[i].adresse_postale_societe)
    doc.text(120, 80, "Numéro de TVA: FRXX999999999")
    doc.text(15, 200, "Moyens de paiement :")
    doc.text(60, 200, "Virement bancaire ou chèque à l'attention de Kertechnologie")
    doc.text(60, 205, "Paypal: exemple@paypal.com")
    doc.text(60, 210, "Banque: Banque Exemple")
    doc.text(60, 215, "SWIFT/BIC: EXAMPL33XXX")
    doc.text(60, 220, "IBAN: FR 99 9999 9999 9999 9999")
    doc.text(15, 230, "Conditions de paiement :")
    doc.text(60, 230, this.state.factureById.condition_paiement)
    doc.text(15, 240, "Numéro d’engagement :")
    doc.text(60, 240, this.state.factureById.num_engagement)
    doc.text(92, 273, "Kertechnologie")
    doc.text(84, 279, "264 Avenue Victor Hugo")
    doc.text(82, 285, "94120 Fontenay-sous-Bois")
    doc.setFontSize(14)
    doc.setFontType('bold')
    doc.text(120, 45, this.state.factureById.client.toUpperCase())
    doc.text(156, 14, 'Facture - ')
    doc.text(178, 14, this.state.factureById.num_facture)
    doc.setFontSize(11)
    doc.text(150, 182, "TOTAL (TTC)")
    doc.text(180, 182, this.state.factureById.montant+"$")
  }
}
});
}


generatePDF(idfacture, index){
  axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
  axios.get("http://45.9.188.225:8080/cra/AllClients")
    .then(response => response.data)
    .then((data) => {
      this.setState({ clients: data });  
    })
  axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/facture/" + idfacture)
      .then(response => response.data)
      .then((data) => {
        this.setState({ factureById: data });
        this.state.index=index ;         
    for(var i=0; i<this.state.clients.length; i++){
      if(this.state.factureById.client == this.state.clients[i].societe){
        console.log("adresse",this.state.clients[i].adresse_postale_societe)

        
        var num = parseInt(this.state.factureById.nbr_jours)
        var numb = parseInt(this.state.factureById.tjm)
        var dhar = num * numb
        console.log(this.state.dhar)
    if(this.state.factureById.taux_tva == "0%"){
      this.state.egl = 0
    }
    else if(this.state.factureById.taux_tva == "2%"){
      this.state.egl = (dhar*2)/100
    }else if(this.state.factureById.taux_tva == "5%"){
      this.state.egl = (dhar*5)/100
    }else if(this.state.factureById.taux_tva == "10%"){
      this.state.egl = (dhar*10)/100
    }else if(this.state.factureById.taux_tva == "20%"){
      this.state.egl = (dhar*20)/100
    }
    console.log(this.state.egl)
  var abcd = this.state.egl.toString();

  var ttr = this.state.factureById.tjm.split(".")
  console.log(ttr)
    const doc = new jsPDF();
   
    const tableColumn = [ "Consultant", "Période","Quantité","Unité", "TJM","TVA", "Montant"];
    const tableRows = [];
  //  const erta = [this.state.factureById.tjm];
      //this.state.factureById.forEach(ticket => {
     var dto = this.state.factureById.mois +' ' + this.state.factureById.annee;
      const ticketData = [
      //  this.state.factureById.id,
      this.state.factureById.consultant,
      dto,
      this.state.factureById.nbr_jours,
      this.state.factureById.unite,
      this.state.factureById.tjm,
      this.state.factureById.taux_tva,
      this.state.factureById.montant
      ];
      tableRows.push(ticketData);
   // });
  
    doc.autoTable(tableColumn, tableRows, { startY: 135 });
    //const date = Date().split(" ");
   // const dateStr = date[0] + date[1] + date[2] + date[3] + date[4] + date[5] + date[6];
    
    doc.addImage(logoKertech.base64, 'JPG', 11, 10, 27, 27)
    doc.setFontSize(10)
   // doc.text(20,160,10,"test + ");
    doc.text(150, 170, "TOTAL HT")
    doc.text(180, 170, dhar+"€")
    doc.text(150, 175, "TVA ("+this.state.factureById.taux_tva+")")
    doc.text( abcd+"€",182, 175)
  
    doc.text(this.state.factureById.message , 15, 125);
    doc.text(15, 45, "Kertechnologie")
    doc.text(15, 50, "Khaled ERJILI")
    doc.text(15, 55, "264 Avenue Victor Hugo")
    doc.text(15, 60, "94120 Fontenay-sous-Bois")
    doc.text(15, 65, "0123456789")
    doc.text(15, 70, "contact@kertechnologie.fr")
    doc.text(15, 75, "www.kertechnologie.fr")
    doc.text(130, 20, "Date de Facturation :")
    doc.text(165, 20, this.state.factureById.date_facture)
    doc.text(120, 50, this.state.clients[i].adresse_postale_societe)
    doc.text(120, 80, "Numéro de TVA: FRXX999999999")
    doc.text(15, 200, "Moyens de paiement :")
    doc.text(60, 200, "Virement bancaire ou chèque à l'attention de Kertechnologie")
    doc.text(60, 205, "Paypal: exemple@paypal.com")
    doc.text(60, 210, "Banque: Banque Exemple")
    doc.text(60, 215, "SWIFT/BIC: EXAMPL33XXX")
    doc.text(60, 220, "IBAN: FR 99 9999 9999 9999 9999")
    doc.text(15, 230, "Conditions de paiement :")
    doc.text(60, 230, this.state.factureById.condition_paiement)
    doc.text(15, 240, "Numéro d’engagement :")
    doc.text(60, 240, this.state.factureById.num_engagement)
    doc.text(90, 273, "Kertechnologie")
    doc.text(84, 279, "264 Avenue Victor Hugo")
    doc.text(82, 285, "94120 Fontenay-sous-Bois")
    doc.setFontSize(14)
    doc.setFontType('bold')
    doc.text(120, 45, this.state.factureById.client.toUpperCase())
    doc.text(156, 14, 'Facture - ')
    doc.text(178, 14, this.state.factureById.num_facture)
    doc.setFontSize(11)
    doc.text(150, 182, "TOTAL (TTC)")
    doc.text(180, 182, this.state.factureById.montant+"€")
    doc.save(`facture_${this.state.factureById.client}.pdf`);
  }
}
});
}


handleChangeMonth(event) {    
    var today=new Date();
    var i = 0;
    var pr =0;
    var format_date= this.formatDate(today);
    this.state.selectedMonth=event.target.value;
    console.log("deb", this.state.selectedMonth)           

    this.setState({selectedMonth:this.state.selectedMonth})
    var db = this.state.selectedYear;
    var wd=this.WorkingDays(this.state.selectedMonth,this.state.selectedYear)
   // console.log("wd",this.weekdays);
    this.state.affectation.forEach(tasks => {
        if (this.convert_to_front(format_date,tasks.date_tache).getMonth()==event.target.value) {
            if (this.convert_to_front(format_date,tasks.date_tache).getFullYear()==this.state.selectedYear){
            i++;
            pr =((i*100)/wd).toFixed(2)
        }}            
        this.setState({dayOfWork:i})        
    })
}

handleChangeYear(event){
    // console.log(event.target.value)
    this.selectedYear=event.target.value; 
    this.setState({selectedYear:this.selectedYear})
 //   console.log("a",this.selectedYear)

}

convert_to_front(date,backDate) {
    
    var t = new Date(date);
    const localOffset2 = t.getTimezoneOffset() * 60000;
    var timestamp2 = (new Date(backDate).getTime() - localOffset2);
    var date = new Date(timestamp2); 
    return date;

}

formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

monthNameToNum(monthname) {
    var months = [
        'Janvier', 'Février', 'Mars', 'Avril', 'Mai',
        'Juin', 'Juillet', 'Août', 'Septembre',
        'Octobre', 'Novembre', 'Décembre'
    ];
    var month = months.indexOf(monthname);
    return month ? month + 1 : 0;
}



WorkingDays(month, year) {
    var days = 32 - new Date(year, month, 32).getDate();
    var weekdays = 0;

    for (var i = 0; i < days; i++) {
        var day = new Date(year, month, i + 1).getDay();
        if (day != 0 && day != 6) weekdays++;
    }    
    this.setState({weekdays});    
    return weekdays;
}
System_Date() {

  var date = new Date()
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var jour = date.getDate()
  var heure = date.getHours()
  var minutes = date.getMinutes();
  var sys_date = 'document géneré le : ' + jour + '-' + month + '-' + year + ' à ' + heure + ':' + minutes;
  return sys_date
} 

 
  render() {
    return (
        <div>
         <NavigationBar/>
          <div className="container">
            <div>
              <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddFacture()}>
                <i className="fas fa-plus-circle"></i> Ajouter une nouvelle facture
              </button>
              {this.state.message != "" ? this.state.message : ""}
            </div>
           
            <table className="table table-sm" >
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col"><i class="fas fa-industry"></i> Numero</th>
                  <th scope="col"><i class="fas fa-user"></i> Client</th>
                  <th scope="col"><i class="fas fa-phone-alt"></i> Crée le </th>
                  <th scope="col"><i class="fas fa-briefcase"></i> Num d'engagement</th>
                  <th scope="col"><i class="fas fa-envelope"></i> Total</th>
                  
                  <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
                </tr>
              </thead>
              <tbody>                
               {this.state.factures.map((facture,index) => (
                  <tr >
                    <td>{facture.idfacture}</td>
                    <td>{facture.num_facture} </td>
                    <td>{facture.client}</td> 
                    <td>{facture.date_facture}</td>
                    <td>{facture.num_engagement}</td>
                    <td>{facture.montant}</td>
                    <td>
                      <div class="btn-group">
                        <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(facture.idfacture,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                        <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(facture.idfacture,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                        <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(facture.idfacture,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                        <button className="btn btn-success" data-toggle="modal" onClick={() => this.generatePDF(facture.idfacture,index)} data-target="#exampleModalTask"> Fichier PDF</button>
                      </div>
                    </td>
                  </tr>))}
              </tbody>
            </table>
          </div>

          <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Facture du client: {this.state.factureById.client} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
              <div class="abcT abcdD">Facture - {this.state.factureById.num_facture}</div>
              <div class="abcX">Date de Facturation : {this.state.factureById.date_facture}</div>
               <img src={logo} class="abcH" alt="logo" />
               <div class="col-md-12 alin abcK">
                 <div class="col-md-6 abcdC">
               <div>Kertechnologie</div>
               <div>Khaled ERJILI</div>
               <div>264 Avenue Victor Hugo</div>
               <div>94120 Fontenay-sous-Bois</div>
               <div>0123456789</div>
               <div>contact@kertechnologie.fr</div>
               <div>www.kertechnologie.fr/</div>
               </div>
               <div class="col-md-6">
               <div class="abcW abcdD">{this.state.factureById.client}</div>
               <div class="abcdC">
                 {this.state.clients.map(item =>
                  item.societe == this.state.factureById.client ?
                  <div key={item.societe}>{item.adresse_postale_societe}</div>
                  : <div> </div>
                  )}
               </div>
               <div class="abcdE">Numéro de TVA: FRXX999999999</div>
               </div>
               </div>
               <div class="abcN">{this.state.factureById.message}</div>

               <table className="table table-sm abcI" >
              <thead>
                <tr class="abcB">
                  <th class="abcC"> Consultant</th>
                  <th class="abcC"> Période</th>
                  <th class="abcC"> Quantité </th>
                  <th class="abcC"> Unité </th>
                  <th class="abcC"> TJM</th>
                  <th class="abcC"> TVA</th>
                  <th class="abcC"> Montant</th>
                </tr>
              </thead>
              <tbody>                
                  <tr >
                    <td>{this.state.factureById.consultant}</td>
                    <td>{this.state.factureById.mois} {this.state.factureById.annee}</td>
                    <td>{this.state.factureById.nbr_jours}</td> 
                    <td>{this.state.factureById.unite}</td>
                    <td>{this.state.factureById.tjm}</td>
                    <td>{this.state.factureById.taux_tva}</td>
                    <td>{this.state.factureById.montant}</td>
                  </tr>
              </tbody>
            </table>

            <div class="abcdF">
            <div class="alin abcdC">
              <div>TOTAL HT</div>
              <div class="abcdG">{this.state.abcdF}€</div>
            </div>
            <div class="alin abcdC">
              <div>TVA ({this.state.factureById.taux_tva})</div>
              <div class="abcdK">{this.state.egl}€</div>
            </div>
            <div class="alin abcdI">
              <div>TOTAL (TTC)</div>
              <div class="abcdH">{this.state.factureById.montant}€</div>
            </div>
            </div>

            <div class="col-md-12 alin abcdC abcG">
              <div class="col-md-4">
              <div>Moyens de paiement : </div>
              </div>
              <div>
                <div class="abcM">
                Virement bancaire ou chèque à l'attention de Kertechnologie<br/>Paypal: exemple@paypal.com<br/>
                Banque: Banque Exemple<br/>SWIFT/BIC: EXAMPL33XXX<br/>IBAN: FR 99 9999 9999 9999 9999
                </div>
              </div>
            </div>
            <div class="col-md-12 alin abcdC abcP">
              <div class="col-md-4">
              <div>Conditions de paiement : </div>
              </div>
              <div>
                <div class="abcM">
                {this.state.factureById.condition_paiement}
                </div>
              </div>
            </div>
            <div class="col-md-12 alin abcdC abcP">
              <div class="col-md-4">
              <div>Numéro d’engagement : </div>
              </div>
              <div>
                <div class="abcM">
                {this.state.factureById.num_engagement}
                </div>
              </div>
            </div>
            <div class="abcO abcdC">
                <div class="abcdA">Kertechnologie</div>
                <div class="abcdB">264 Avenue Victor Hugo</div>
                <div>94120 Fontenay-sous-Bois</div>
            </div>
           
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
   
          <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content wdar">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.factureById.client} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <div class="form-group col-md-4">
                      <label for="exampleFormControlSelect1">Client</label>
                      <select required class="form-control" value={this.state.factureById.client} onChange={this.handleChange.bind(this, "client")}>
                        <option value="">Selectionnez ... </option>
                        {this.state.clients.map(client => (
                        <option key={client.id} > {client.societe} </option>))}
                      </select>                
                    </div>
                    
                    <div class="starl"></div>
                    <div class="col-sm-12 alin sntri">
                    <div class="col-sm-6">
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Numéro de facture</label>
                      <input  type="text" class="form-control" value={this.state.factureById?.num_facture || ''}
                       onChange={this.handleChange.bind(this, "num_facture")} 
                       placeholder="insérez le numéro de la facture"></input>
                   </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Date de facturation</label> 
                      <input type="date" class="form-control" value={this.state.factureById.date_facture || ''} onChange={this.handleChange.bind(this, "date_facture")} placeholder="insérez le numéro du téléphone du client"></input>
                    </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Condition de paiement</label>
                      <select required class="form-control" value={this.state.factureById.condition_paiement || ''} onChange={this.handleChange.bind(this, "condition_paiement")}>
                        <option value=""></option>
                        <option value="1 jours">1 jour</option>
                        <option value="7 jours">7 jours</option>
                        <option value="15 jours">15 jours</option>
                        <option value="30 jours">30 jours</option>
                        <option value="60 jours">60 jours</option>
                      </select>                
                    </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Numéro d'engagement</label>
                      <input type="text" class="form-control" value={this.state.factureById.num_engagement || ''} onChange={this.handleChange.bind(this, "num_engagement")} placeholder="insérez le numéro"></input>
                    </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Code service</label>
                      <input type="text" class="form-control" value={this.state.factureById.code_service} onChange={this.handleChange.bind(this, "code_service")} placeholder="insérez le code"></input>
                    </div>
                    </div>
                    <div class="col-sm-5 desc">
                    <div class="form-group">
                      <label for="exampleFormControlSelect1" class="qimzq">Message</label>
                      <input type="textarea" class="form-control arcv" value={this.state.factureById.message} onChange={this.handleChange.bind(this, "message")} placeholder="saisir un message ..."></input>
                    </div>
                    </div>
                    </div>
                    <div class="lines"></div>
                    <div class="col-md-12 alin ratp" onLoad={this.OptionsSelect()}>
                    <div class="form-group col-md-3">
                    <label for="exampleFormControlSelect1" class="qimzq">Prestation</label>
                      <input type="text" class="form-control" value={this.state.factureById.consultant} onChange={this.handleChange.bind(this, "consultant")} placeholder="insérer le consultant"></input>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">  Veuillez selectionner une année </label>
                      <select className="form-control" value={this.state.selectedYear} onChange={this.handleChangeYear.bind(this)}>
                            <option  value=""></option>
                            {this?.state.option_year}
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                       <label for="exampleFormControlSelect1" class="labla qimzq"> Veuillez selectionner un mois</label>
                       <select className="form-control" value={this.state.selectedMonth} onChange={this.handleChangeMonth.bind(this)}>
                            <option  value=""></option>
                            {this?.state.option_month}
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Quantité</label>
                      <input type="text" class="form-control" value={this.state.quantite} onChange={eqt => this.updateQantiteValue(eqt)}/>
                    </div>
                    </div>
                    <div class="lines"></div>
                    <div class="col-md-12 alin sncf">
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Unité</label>
                      <select required class="form-control" value={this.state.factureById.unite} onChange={this.handleChange.bind(this, "unite")}>
                        <option value=""></option>
                        <option value="h">heure</option>
                        <option value="jour">jours</option>
                      </select>                
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">TJM</label>
                      <input type="text" class="form-control" value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} />
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Taux de TVA</label>
                      <select required class="form-control" value={this.state.tvtau} onChange={eat => this.updateSelectValue(eat)}>
                        <option value="0%">0%</option>
                        <option value="2%">2%</option>
                        <option value="5%">5%</option>
                        <option value="10%">10%</option>
                        <option value="20%">20%</option>
                      </select>  
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Montant</label>
                      <input disabled = {true} type="text" class="form-control" value={this.state.som} ></input>
                    </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateFacture.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        
          <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document" >
              <div class="modal-content wdar">
                <div class="modal-header modalheader">
                  <h5 class="modal-title" id="exampleModalLabel"> Nouveau Facture </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body modal_items_position" >
                  <form>
                    <div class="form-group col-md-4">
                      <label for="exampleFormControlSelect1">Client</label>
                      <select required class="form-control" value={this.state.factureById.client} onChange={this.handleChange.bind(this, "client")}>
                        <option value="">Selectionnez ... </option>
                        {this.state.clients.map(client => (
                        <option key={client.id} > {client.societe} </option>))}
                      </select>                
                    </div>
                    <div class="starl"></div> 
                    <div class="col-sm-12 alin sntri">
                    <div class="col-sm-6">
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Numéro de facture</label>
                      <input  type="text" class="form-control" value={this.state.factureById.num_facture}
                       onChange={this.handleChange.bind(this, "num_facture")} 
                       placeholder="insérez le numéro de la facture"></input>
                   </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Date de facturation</label> 
                      <input type="date" class="form-control" value={this.state.factureById.date_facture} onChange={this.handleChange.bind(this, "date_facture")} placeholder="insérez le numéro du téléphone du client"></input>
                    </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Condition de paiement</label>
                      <select required class="form-control" value={this.state.factureById.condition_paiement} onChange={this.handleChange.bind(this, "condition_paiement")}>
                        <option value=""></option>
                        <option value="1 jours">1 jour</option>
                        <option value="7 jours">7 jours</option>
                        <option value="15 jours">15 jours</option>
                        <option value="30 jours">30 jours</option>
                        <option value="60 jours">60 jours</option>
                      </select>                
                    </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Numéro d'engagement</label>
                      <input type="text" class="form-control" value={this.state.factureById.num_engagement} onChange={this.handleChange.bind(this, "num_engagement")} placeholder="insérez le numéro"></input>
                    </div>
                    <div class="form-group alin">
                      <label for="exampleFormControlSelect1" class="col-md-4 sizm">Code service</label>
                      <input type="text" class="form-control" value={this.state.factureById.code_service} onChange={this.handleChange.bind(this, "code_service")} placeholder="insérez le code"></input>
                    </div>
                    </div>
                    <div class="col-sm-5 desc">
                    <div class="form-group">
                      <label for="exampleFormControlSelect1" class="qimzq">Message</label>
                      <input type="textarea" class="form-control arcv" value={this.state.factureById.message} onChange={this.handleChange.bind(this, "message")} placeholder="saisir un message ..."></input>
                    </div>
                    </div>
                    </div>
                    <div class="lines"></div>
                    <div class="col-md-12 alin ratp" onLoad={this.OptionsSelect()}>
                    <div class="form-group col-md-3">
                    <label for="exampleFormControlSelect1" class="qimzq">Prestation</label>
                      <input type="text" class="form-control" value={this.state.factureById.consultant} onChange={this.handleChange.bind(this, "consultant")} placeholder="insérer le consultant"></input>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">  Veuillez selectionner une année </label>
                      <select className="form-control" value={this.state.selectedYear} onChange={this.handleChangeYear.bind(this)}>
                            <option  value=""></option>
                            {this?.state.option_year}
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                       <label for="exampleFormControlSelect1" class="labla qimzq"> Veuillez selectionner un mois</label>
                       <select className="form-control" value={this.state.selectedMonth} onChange={this.handleChangeMonth.bind(this)}>
                            <option  value=""></option>
                            {this?.state.option_month}
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Quantité</label>
                      <input type="text" class="form-control" value={this.state.quantite} onChange={eqt => this.updateQantiteValue(eqt)}/>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="lines"></div>
                    <div class="col-md-12 alin sncf">
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Unité</label>
                      <select required class="form-control" value={this.state.factureById.unite} onChange={this.handleChange.bind(this, "unite")}>
                        <option value=""></option>
                        <option value="h">heure</option>
                        <option value="jour">jours</option>
                      </select>                
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">TJM</label>
                      <input type="text" class="form-control" value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} placeholder="000.00" />
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Taux de TVA</label>
                      <select required class="form-control" value={this.state.tvtau} onChange={eat => this.updateSelectValue(eat)}>
                        <option value="0%">0%</option>
                        <option value="2%">2%</option>
                        <option value="5%">5%</option>
                        <option value="10%">10%</option>
                        <option value="20%">20%</option>
                      </select>  
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlSelect1" class="labla qimzq">Montant</label>
                      <input  type="text" class="form-control" value={this.state.som} ></input>
                    </div>
                   
                    </div>
                    
                    {this.createUI()}        
                       <button class="ajt" onClick={this.addClick.bind(this)}><i class="fa fa-plus pls"></i></button>
                       
                    </div>
                    
                  </form>
                </div>
  
                <div class="modal-footer modalfooter">
                  <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateFacture.bind(this)} data-dismiss="modal">Ajouter le facture</button>
                  <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
                </div>
              </div>
            </div>
          </div>
            
          <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion Facture</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer la facture du client : {this.state.factureById.client}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteFacture.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
     
      <div className="row view-cra">
                    <div className="col-1"></div>
                </div>
      </div>
  
      );

  }
}  
export default facture;