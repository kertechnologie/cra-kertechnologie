import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'

class ESN extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      esns:[],
      esnById:{},
      index:null,
      values: [],
      consults:[],
      mails : [],
      tlfn : [],
      postls:[],
      query: "",
      data: [],
      users:[],
      filteredData: [],
      dv : [],
      sam : ''
    };
  }

  handleChange(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case 'societe': {
            this.state.esnById.societe = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;    
          case 'tel': {
            this.state.esnById.tel = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'poste': {
            this.state.esnById.poste = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'email': {
            this.state.esnById.email = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'interlocuteur': {
            this.state.esnById.interlocuteur = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'commentaire': {
            this.state.esnById.commentaire = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break; 
          case (i): {
            let values = [...this.state.values];
              values[i] = e.target.value;
              this.setState({  values  });             
          }       
          break;  
          default: 
          break;
        }   
      }
    }
  }

  handleChanEmail(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let mails = [...this.state.mails];
            mails[i] = e.target.value;
            this.setState({  mails  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  handleChanTlfn(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let tlfn = [...this.state.tlfn];
            tlfn[i] = e.target.value;
            this.setState({  tlfn  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  handleChanPost(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let postls = [...this.state.postls];
            postls[i] = e.target.value;
            this.setState({  postls  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  createUI(){
    return this.state.values.map((el, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre nom de l'interlocuteur" value={el||''} class="form-control" onChange={this.handleChange.bind(this, i)} />       
        <button value='remove' class="detr" onClick={this.removeClick.bind(this, i)}>
        <i class="fa fa-minus sam"></i></button>           
      </div>   
    )
  }

  addClick(){
    this.setState(prevState => ({ values: [...prevState.values, '']}))
  }
  
  removeClick(i){
     let values = [...this.state.values];
     values.splice(i,1);
     this.setState({ values });
  }

  Maillt(){
    return this.state.mails.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre email de l'ESN" value={item||''} class="form-control"
          onChange={this.handleChanEmail.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removeEmail.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addEmailt(){
    this.setState(prevState => ({ mails: [...prevState.mails, '']}))
  }

  removeEmail(i){
    let mails = [...this.state.mails];
    mails.splice(i,1);
    this.setState({ mails });
  }

  Telephn(){
    return this.state.tlfn.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre numéro du téléphone" value={item||''} class="form-control"
          onChange={this.handleChanTlfn.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removeTelefn.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addTelefn(){
    this.setState(prevState => ({ tlfn: [...prevState.tlfn, '']}))
  }

  removeTelefn(i){
    let tlfn = [...this.state.tlfn];
    tlfn.splice(i,1);
    this.setState({ tlfn });
  }

  posteInit(){
    return this.state.postls.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre poste de l'interlocuteur" value={item||''} class="form-control"
          onChange={this.handleChanPost.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removePostes.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addPostes(){
    this.setState(prevState => ({ postls: [...prevState.postls, '']}))
  }

  removePostes(i){
    let postls = [...this.state.postls];
    postls.splice(i,1);
    this.setState({ postls });
  }

  componentDidMount() {    
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        for(var i=0; i<data.length; i++){
          this.setState({users:data})   
        }
      })   
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllEsns")
      .then(response => response.data)
      .then((data) => {
        this.setState({ esns: data });  
      });
  }

  componentWillMount() {
    this.componentDidMount();
  }

  AddEsn(){
    this.state.esnById.societe = '';
    this.state.esnById.interlocuteur = '';
    this.state.esnById.tel = '';
    this.state.esnById.poste = '';
    this.state.esnById.email = '';
    this.state.esnById.commentaire = '';
    this.setState({ esnById: this.state.esnById })
  } 

  // ajouter une SSII
  ValidateEsn() { 
    var nom =this.state.values.join(' - ')
    var ymail = this.state.mails.join(' - ')
    var tlifn = this.state.tlfn.join(' - ')
    var pstl = this.state.postls.join(' - ')
    this.state.esnById.interlocuteur = this.state.esnById.interlocuteur + ' - ' + nom
    this.state.esnById.email = this.state.esnById.email + ' - ' + ymail
    this.state.esnById.tel = this.state.esnById.tel + ' - ' + tlifn
    this.state.esnById.poste = this.state.esnById.poste + ' - ' + pstl

    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/aEsn", this.state.esnById) 
      .then(response => response.data)
      .then((esnById) => {
        this.state.esns.push(esnById);       
        this.setState({users:this.state.esns});
      //  window.location.reload(false);
      }).catch((error) => { console.log(error) 
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        un probléme detetcté lors de l'insertion de votre nouvelle Esn</p>
        this.setState({message:this.state.message})
      });
  }

  //supprimer une SSII
  DeleteEsn(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/deleteEsn/"+this.state.esnById.idesn)
      .then(response => response.data)
      .then((data) => {
        this.state.esns.splice(this.state.index,1);
        this.setState({users:this.state.esns});
       // window.location.reload(false);
      }).catch((error) => { console.log(error) });
  }

  //lire une SSII ( pas de tout les SSII)
  handleSort(idesn,index) {
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/esn/" + idesn)
      .then(response => response.data)
      .then((data) => {
        this.state.values = data.interlocuteur.split(' - ')
        this.state.mails = data.email.split(' - ')
        this.state.tlfn = data.tel.split(' - ')
        this.state.postls = data.poste.split(' - ')
        this.setState({ esnById: data });
        // this.state.esnById.interlocuteur = this.state.esnById.interlocuteur.split(' - ')
        this.state.index=index ;            
      });
  }

  //modifier une SSII
  UpdateEsn(){
    var nom =this.state.values.join(' - ')
    this.state.esnById.interlocuteur = nom
    this.state.esnById.email = this.state.mails.join(' - ')
    this.state.esnById.tel = this.state.tlfn.join(' - ')
    this.state.esnById.poste = this.state.postls.join(' - ')
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/updateEsn",this.state.esnById)
      .then(response => response.data)
      .then((data) => {
        this.state.esns.splice(this.state.index,1);
        this.state.esns.push(data);
        this.setState({esns:this.state.esns});
        window.location.reload(false);
      }).catch((error) => { console.log(error)
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        votre modification est echouée</p>
        this.setState({message:this.state.message}) 
      });
  }

  render() {
    return (
      <div class="backtotal">
       <NavigationBar/>
       
        <div className="container">
            
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddEsn()}>
              <i className="fas fa-plus-circle"></i> Ajouter une nouvelle ESN
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
          <table className="table table-sm" >
            <thead>
              <tr>
                <th scope="col" hidden={true}></th>
                <th scope="col"><i class="fas fa-industry"></i> Nom</th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Telephone </th>
                <th scope="col"><i class="fas fa-envelope"></i> Email</th>
                <th scope="col"><i class="fas fa-user"></i> Interlocuteur</th>
                <th scope="col"><i class="fas fa-briefcase"></i> Poste</th>
                <th scope="col"><i class="fas fa-comments"></i> Commentaire</th>

                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody> 
             {this.state.esns.map((esn,index) => (
                <tr >
                  <td hidden={true}>{esn.idesn}</td>
                  <td>{esn.societe} </td>
                  <td>{esn.tel.length>3?
                  esn.tel.split(' - ').map(item => {return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td>
                  <td>{esn.email.length>3?
                  esn.email.split(' - ').map(item => {return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td>
                  <td>{esn.interlocuteur.length>3?
                   esn.interlocuteur.split(' - ').map(item => { return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td> 
                  <td>{esn.poste.length>3?
                  esn.poste.split(' - ').map(item => {return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td>
                  <td> {esn.commentaire.length>=20? 
                          <div key={esn.commentaire.length}>{esn.commentaire.substring(0, 20) + ' ...'}</div> 
                        : <div>{esn.commentaire} </div>
                       }
                  </td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données de l'ESN: {this.state.esnById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-address-card"></i> Société</td>
                      <td>{this.state.esnById.societe}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Telephone </td>
                      <td>{ this.state.tlfn.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))}
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Email </td>
                      <td>
                      { this.state.mails.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))
                        }
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-user"></i> Interlocuteur </td>
                     <td> 
                        { this.state.values.map((item) => (     
                          item.split(' - ').map(item => { return (<div> {item} </div>)})))
                        }
                     </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Poste </td>
                      <td>{ this.state.postls.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))}
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-comments"></i> Commentaire</td>
                      <td>{this.state.esnById.commentaire}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.esnById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input required type="text" class="form-control" value={this.state.esnById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le nom de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label>
                    {this.Telephn()}        
                       <div type="button" class="ajt" onClick={this.addTelefn.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    {this.Maillt()}        
                       <div type="button" class="ajt" onClick={this.addEmailt.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                       {this.createUI()}        
                       <div type="button" class="ajt" onClick={this.addClick.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    {this.posteInit()}        
                       <div type="button" class="ajt" onClick={this.addPostes.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input required type="text" class="form-control" value={this.state.esnById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateEsn.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter une ESN </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input type="text" class="form-control" value={this.state.esnById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le nom de l'ESN"></input>
                  </div>
                 
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label> 
                    <input type="text" class="form-control" value={this.state.esnById.tel} onChange={this.handleChange.bind(this, "tel")} placeholder="insérez le telephone de l'ESN"></input>
                    {this.Telephn()}        
                     <div type="button" class="ajt" onClick={this.addTelefn.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.esnById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez l'email de l'ESN"></input>
                    {this.Maillt()}        
                    <div type="button" class="ajt" onClick={this.addEmailt.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                    <input  type="text" class="form-control" value={this.state.esnById.interlocuteur}
                     onChange={this.handleChange.bind(this, "interlocuteur")} 
                     placeholder="insérez le nom de l'interlocuteur"></input>
                       {this.createUI()}        
                       <div type="button" class="ajt" onClick={this.addClick.bind(this)}><i class="fa fa-plus pls"></i></div>
                 </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    <input type="text" class="form-control" value={this.state.esnById.poste} onChange={this.handleChange.bind(this, "poste")} placeholder="insérez le poste de l'interlocuteur"></input>
                    {this.posteInit()}        
                     <div type="button" class="ajt" onClick={this.addPostes.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input type="text" class="form-control" value={this.state.esnById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateEsn.bind(this)} data-dismiss="modal">Ajouter l'ESN</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion ESN</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer l'ESN : {this.state.esnById.societe}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteEsn.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      </div>

    );
  }
}  
export default ESN;