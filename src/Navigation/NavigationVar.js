import React from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import {parseJwt} from '../util/JWParser' ;


import { Redirect } from 'react-router-dom';
class NavigationBar extends React.Component {
  constructor(propos) {
    super(propos);
    this.state = {
      users: [],
      redirect: false,
      dropdown : [],
      init : false,
      conges:[],
      gestcom:[],
      commer:[],
      rhcom:[],
      fact:'',
      cra_d:[]
    };
  }
  logout() {

    sessionStorage.removeItem("token")
    localStorage.removeItem("token")

  }
  componentDidMount() {


    var decoded = parseJwt(sessionStorage.getItem("token")) ;
    
    if(decoded.exp  < new Date().getTime() / 1000) {
      this.setState({redirect:true}) ;
    } else {
      this.setState({users:decoded}) ;
    }
    


  }

  render() {

    if (this.state.redirect) {
     
      sessionStorage.setItem("redirectTo",window.location.href.split("3000/")[1].split("?")[0]);

      return <Redirect to='/'/>

    }

    if(( (this.state.users.role=="Admin") || (this.state.users.role=="Utilisateur")) &&(this.state.init===false)){
        this.state.dropdown.push(
      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle navlibelle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Gestion des CRA
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <Link to="calendrier">
        <a class="dropdown-item" href="#">Saisir CRA</a>
        </Link>
        <a class="dropdown-item" href="#">Consulter les CRA </a> 
      </div>
      </li>
        )
        this.setState({cra_d:[]})
      this.setState({init:true})
    }

    if((this.state.users.role=="Admin") &&(this.state.init===false)){
      this.state.dropdown.push(
        
    <Link to="facture">
    <li className="nav-item">
      <a className="nav-link navlibelle"  id="profil" href="/#">Facture</a>
    </li>
  </Link>
    )
    this.setState({fact:[]})
    this.setState({init:true})
  }

    if((this.state.users.role=="Admin") &&(this.state.init===false)){
      this.state.dropdown.push(
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle navlibelle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Gestion Commerciale
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <Link to="candidat">
              <a class="dropdown-item" href="#">Candidats</a>
            </Link>
            <Link to="client">
            <a class="dropdown-item" href="#">Clients</a>
            </Link>
            <Link to="ESN">
            <a class="dropdown-item" href="#">ESN</a>
            </Link>
            <Link to="prospect">
            <a class="dropdown-item" href="#">Prospects</a>
            </Link>
            <Link to="opportunite">
            <a class="dropdown-item" href="#">Opportunites</a>
            </Link>
           
          </div>
      </li>
    )
    this.setState({commer:[]})
    this.setState({init:true})
  }

    if((this.state.users.role=="Admin") &&(this.state.init===false)){
        this.state.dropdown.push(
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle navlibelle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Administration
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link to="users">
                <a class="dropdown-item" href="#">Gestion des utilisateurs</a>
              </Link>
              <Link to="consultant">
                <a class="dropdown-item" href="#">Consultants</a>
              </Link>
              <Link to="projet">
              <a class="dropdown-item" href="#">Gestion des projets</a>
              </Link>
              <Link to="cra">
              <a class="dropdown-item" href="#">Consulter les CRA</a>
              </Link>
              <Link to="gestion_conges">
              <a class="dropdown-item" href="#">Gestion des conges</a>
              </Link>
            </div>
        </li>
      )
      this.setState({conges:[]})
      this.setState({init:true})
    }
    
    else if ((this.state.users.role=="RH")&&(this.state.init===false)){
        this.state.dropdown.push(
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle navlibelle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Service RH
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link to="users">
                <a class="dropdown-item" href="#">Gestion des utilisateurs</a>
              </Link>
              <Link to="conges">
              <a class="dropdown-item" href="#">Gestion des conges</a>
              </Link>
              <Link to="candidat">
                <a class="dropdown-item" href="#">Candidats</a>
              </Link>
              <Link to="client">
              <a class="dropdown-item" href="#">Clients</a>
              </Link>
              <Link to="prospect">
              <a class="dropdown-item" href="#">Prospects</a>
              </Link>
            </div>
        </li>
              )    
      this.setState({rhcom:[]})
      this.setState({init:true}) 
    }
    
    else if ((this.state.users.role=="Commercial")&&(this.state.init===false)){
        this.state.dropdown.push(
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle navlibelle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Service Commerciale
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link to="candidat">
                <a class="dropdown-item" href="#">Candidats</a>
              </Link>
              <Link to="client">
              <a class="dropdown-item" href="#">Clients</a>
              </Link>
              <Link to="prospect">
              <a class="dropdown-item" href="#">Prospects</a>
              </Link>
              <Link to="consultant">
                <a class="dropdown-item" href="#">Consultants</a>
              </Link>
              <Link to="opportunite">
                <a class="dropdown-item" href="#">Opportunites</a>
              </Link>
            </div>
          </li>
        )    
      this.setState({gestcom:[]})
      this.setState({init:true}) 
    }
    
    else if ((this.state.users.role!="Admin")&&(this.state.users.role!="RH")){
        this.state.conges.push(<Link to="conges">
              <li className="nav-item">
                <a className="nav-link navlibelle" href="/#" id="logout">Gestion congés</a>
              </li>
            </Link>)     
    }
    
   /* <nav className="navbar navbar-expand-lg navbar-dark bg-primary" id="navbar" width="100%" */
    return (
      <nav className="navbar navbar-expand-lg navbar-dark nbt" id="navbar" width="100%">
        <a className="navbar-brand logo" href="/calendrier">       
          <img src="./Kertech.png" id="logo-nav" width="80px" height="25px"></img>
            </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav col-4">
           
           <Link style={{display: "flex"}}>{this.state.cra_d}
           </Link>
            <Link to="profil">
              <li className="nav-item">
                <a className="nav-link navlibelle"  id="profil" href="/#">Profil</a>
              </li>
            </Link>
            {this.state.fact}
            {this.state.rhcom}
            {this.state.gestcom}
            {this.state.conges}
            <Link style={{display: "flex"}}>{this.state.commer}</Link>
            <Link style={{display: "inherit"}}>
            {this.state.dropdown}
            </Link>
            
          </ul>
          
          <ul className="navbar-nav col-4"> </ul>
         
          <ul className="navbar-nav col-4">
            <Link to="/">
              <li className="nav-item">
                <a className="nav-link navlibelle" href="/#" id="logout" onClick={this.logout.bind(this)}>Déconnexion</a>
              </li>
            </Link>
            <li className="nav-item">
              <a className="nav-link navlibelle" id="user" href="/#" >{this.state.users.prenom} {this.state.users.nom} ({this.state.users.role})</a>
            </li>
            <Link to="pwd">
              <li className="nav-item">
                <a className="nav-link navlibelle" href="/#" id="user"><i class="fas fa-users-cog"></i></a>
              </li>
            </Link>
          </ul>
        </div>

      </nav>      

    );
  }
}

export default NavigationBar;