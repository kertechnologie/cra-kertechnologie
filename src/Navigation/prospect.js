import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'

class prospect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prospects:[],
      prospectById:{},
      index:null,
      values: [],
      consults:[],
      mails : [],
      tlfn : [],
      postls:[],
      query: "",
      data: [],
      users:[],
      filteredData: [],
      dv : [],
      sam : ''
    };
  }

  handleChange(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case 'societe': {
            this.state.prospectById.societe = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break;    
          case 'tel': {
            this.state.prospectById.tel = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break;
          case 'poste': {
            this.state.prospectById.poste = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break;
          case 'email': {
            this.state.prospectById.email = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break;
          case 'adresse_postale_societe': {
            this.state.prospectById.adresse_postale_societe = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break;
          case 'activite': {
            this.state.prospectById.activite = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break;
          case 'interlocuteur': {
            this.state.prospectById.interlocuteur = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break;
          case 'commentaire': {
            this.state.prospectById.commentaire = e.target.value;
            this.setState({ prospectById: this.state.prospectById });
          }
          break; 
          case (i): {
            let values = [...this.state.values];
              values[i] = e.target.value;
              this.setState({  values  });             
          }       
          break;  
          default: 
          break;
        }   
      }
    }
  }

  handleChanEmail(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let mails = [...this.state.mails];
            mails[i] = e.target.value;
            this.setState({  mails  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  handleChanTlfn(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let tlfn = [...this.state.tlfn];
            tlfn[i] = e.target.value;
            this.setState({  tlfn  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  handleChanPost(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let postls = [...this.state.postls];
            postls[i] = e.target.value;
            this.setState({  postls  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  createUI(){
    return this.state.values.map((el, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre nom du prospect" value={el||''} class="form-control" onChange={this.handleChange.bind(this, i)} />       
        <button value='remove' class="detr" onClick={this.removeClick.bind(this, i)}>
        <i class="fa fa-minus sam"></i></button>           
      </div>   
    )
  }

  addClick(){
    this.setState(prevState => ({ values: [...prevState.values, '']}))
  }
  
  removeClick(i){
     let values = [...this.state.values];
     values.splice(i,1);
     this.setState({ values });
  }

  Maillt(){
    return this.state.mails.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre email du prospect" value={item||''} class="form-control"
          onChange={this.handleChanEmail.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removeEmail.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addEmailt(){
    this.setState(prevState => ({ mails: [...prevState.mails, '']}))
  }

  removeEmail(i){
    let mails = [...this.state.mails];
    mails.splice(i,1);
    this.setState({ mails });
  }

  Telephn(){
    return this.state.tlfn.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre numéro du téléphone" value={item||''} class="form-control"
          onChange={this.handleChanTlfn.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removeTelefn.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addTelefn(){
    this.setState(prevState => ({ tlfn: [...prevState.tlfn, '']}))
  }

  removeTelefn(i){
    let tlfn = [...this.state.tlfn];
    tlfn.splice(i,1);
    this.setState({ tlfn });
  }

  posteInit(){
    return this.state.postls.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre poste du prospect" value={item||''} class="form-control"
          onChange={this.handleChanPost.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removePostes.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addPostes(){
    this.setState(prevState => ({ postls: [...prevState.postls, '']}))
  }

  removePostes(i){
    let postls = [...this.state.postls];
    postls.splice(i,1);
    this.setState({ postls });
  }

  componentDidMount() {    
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        for(var i=0; i<data.length; i++){
          this.setState({users:data})   
        }
      })   
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllProspects")
      .then(response => response.data)
      .then((data) => {
        const { query } = this.state;
        const filteredData = data.filter(element => {
          return ((element.societe.toLowerCase().includes(query.toLowerCase())) 
              ||(element.poste.toLowerCase().includes(query.toLowerCase()))
              ||(element.interlocuteur.toLowerCase().includes(query.toLowerCase()))) ;
              //.toLowerCase().includes(data[0].poste.toLowerCase())
        });
        this.setState({ data, filteredData});
        this.setState({ prospects: data });  
       // if(filteredData.commentaire.length>=20){
       //     filteredData.commentaire = filteredData.commentaire.substring(0, 10) + ' ...'
      // }else{filteredData.commentaire = filteredData.commentaire}
       console.log("tst", this.state.prospectById.commentaire)
      });
  }

  componentWillMount() {
    this.componentDidMount();
  }

  handleInputChange = event => {
    const query = event.target.value;
    this.setState(prevState => {
      const filteredData = prevState.data.filter(element => {
        return ((element.societe.toLowerCase().includes(query.toLowerCase())) 
              ||(element.poste.toLowerCase().includes(query.toLowerCase()))
              ||(element.interlocuteur.toLowerCase().includes(query.toLowerCase()))) ;
              //.toLowerCase().includes(event.target.value.toLowerCase())
        }); 
        return { query, filteredData };  
    });
  };
  
  componentWillMount() {
    this.componentDidMount();
  }

  AddProspect(){
    this.state.prospectById.societe = '';
    this.state.prospectById.interlocuteur = '';
    this.state.prospectById.tel = '';
    this.state.prospectById.poste = '';
    this.state.prospectById.email = '';
    this.state.prospectById.adresse_postale_societe = '';
    this.state.prospectById.activite = '';
    this.state.prospectById.commentaire = '';
    this.setState({ prospectById: this.state.prospectById })
  } 

  // ajouter un prospect
  ValidateProspect() { 
    var nom =this.state.values.join(' - ')
    var ymail = this.state.mails.join(' - ')
    var tlifn = this.state.tlfn.join(' - ')
    var pstl = this.state.postls.join(' - ')
    this.state.prospectById.interlocuteur = this.state.prospectById.interlocuteur + ' - ' + nom
    this.state.prospectById.email = this.state.prospectById.email + ' - ' + ymail
    this.state.prospectById.tel = this.state.prospectById.tel + ' - ' + tlifn
    this.state.prospectById.poste = this.state.prospectById.poste + ' - ' + pstl

    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/aProspect", this.state.prospectById) 
      .then(response => response.data)
      .then((prospectById) => {
        this.state.prospects.push(prospectById);       
        this.setState({users:this.state.prospects});
        window.location.reload(false);
      }).catch((error) => { console.log(error) 
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        un probléme detetcté lors de l'insertion de votre nouveau Prospect</p>
        this.setState({message:this.state.message})
      });
  }

  //supprimer un prospect
  DeleteProspect(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/deleteProspect/"+this.state.prospectById.idprospect)
      .then(response => response.data)
      .then((data) => {
        this.state.prospects.splice(this.state.index,1);
        this.setState({users:this.state.prospects});
        window.location.reload(false);
      }).catch((error) => { console.log(error) });
  }

  //lire un prospect ( pas de tout les prospects)
  handleSort(idprospect,index) {
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/prospect/" + idprospect)
      .then(response => response.data)
      .then((data) => {
        this.state.values = data.interlocuteur.split(' - ')
        this.state.mails = data.email.split(' - ')
        this.state.tlfn = data.tel.split(' - ')
        this.state.postls = data.poste.split(' - ')
        this.setState({ prospectById: data });
        // this.state.prospectById.interlocuteur = this.state.prospectById.interlocuteur.split(' - ')
        this.state.index=index ;            
      });
  }

  //modifier un prospect
  UpdateProspect(){
    var nom =this.state.values.join(' - ')
    this.state.prospectById.interlocuteur = nom
    this.state.prospectById.email = this.state.mails.join(' - ')
    this.state.prospectById.tel = this.state.tlfn.join(' - ')
    this.state.prospectById.poste = this.state.postls.join(' - ')
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/updateProspect",this.state.prospectById)
      .then(response => response.data)
      .then((data) => {
        this.state.prospects.splice(this.state.index,1);
        this.state.prospects.push(data);
        this.setState({prospects:this.state.prospects});
        window.location.reload(false);
      }).catch((error) => { console.log(error)
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        votre modification est echouée</p>
        this.setState({message:this.state.message}) 
      });
  }

  render() {
    return (
      <div class="backtotal">
       <NavigationBar/>
       
        <div className="container">
            
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddProspect()}>
              <i className="fas fa-plus-circle"></i> Ajouter un nouveau Prospect
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
          <div className="searchForm rr">
            <form>
            <div className="soci"><label>Rechercher par Société, Nom de l'interlocuteur ou par Poste :</label>
             <input class="tstbor" placeholder="Search for..." value={this.state.query} onChange={this.handleInputChange} />
            </div>
            </form>
          </div>
          <table className="table table-sm" >
            <thead>
              <tr>
                <th scope="col" hidden={true}></th>
                <th scope="col"><i class="fas fa-industry"></i> Société</th>
                <th scope="col"><i class="fas fa-user"></i> Interlocuteur</th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Telephone </th>
                <th scope="col"><i class="fas fa-briefcase"></i> Poste</th>
                <th scope="col"><i class="fas fa-envelope"></i> Email</th>
                <th scope="col"><i class="fas fa-tasks"></i> Activité</th>
                <th scope="col"><i class="fas fa-address-card"></i> Adresse Postale Société</th>
                <th scope="col"><i class="fas fa-comments"></i> Commentaire</th>

                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody> 
             {this.state.filteredData.map((prospect,index) => (
                <tr >
                  <td hidden={true}>{prospect.idprospect}</td>
                  <td>{prospect.societe} </td>
                  <td>{prospect.interlocuteur.length>3?
                   prospect.interlocuteur.split(' - ').map(item => { return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td> 
                  <td>{prospect.tel.length>3?
                  prospect.tel.split(' - ').map(item => {return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td>
                  <td>{prospect.poste.length>3?
                  prospect.poste.split(' - ').map(item => {return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td>
                  <td>{prospect.email.length>3?
                  prospect.email.split(' - ').map(item => {return (<div> {item} </div>)})
                  :<div></div>
                  }
                  </td>
                  <td>{prospect.activite}</td>
                  <td>{prospect.adresse_postale_societe}</td>
                  <td> {prospect.commentaire.length>=20? 
                          <div key={prospect.commentaire.length}>{prospect.commentaire.substring(0, 20) + ' ...'}</div> 
                        : <div>{prospect.commentaire} </div>
                       }
                  </td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(prospect.idprospect,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(prospect.idprospect,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(prospect.idprospect,index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(prospect.idprospect,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données du prospect: {this.state.prospectById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-address-card"></i> Société</td>
                      <td>{this.state.prospectById.societe}</td>
                    </tr>

                    <tr>
                      <td><i class="fas fa-user"></i> Interlocuteur </td>
                     <td> 
                        { this.state.values.map((item) => (     
                          item.split(' - ').map(item => { return (<div> {item} </div>)})))
                        }
                     </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Telephone </td>
                      <td>{ this.state.tlfn.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))}
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Poste </td>
                      <td>{ this.state.postls.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))}
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Email </td>
                      <td>
                      { this.state.mails.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))
                        }
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Activité</td>
                      <td>{this.state.prospectById.activite}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Adresse Postale Société</td>
                      <td>{this.state.prospectById.adresse_postale_societe}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-comments"></i> Commentaire</td>
                      <td>{this.state.prospectById.commentaire}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.prospectById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input required type="text" class="form-control" value={this.state.prospectById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le société du prospect"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                       {this.createUI()}        
                       <div type="button" class="ajt" onClick={this.addClick.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label>
                    {this.Telephn()}        
                       <div type="button" class="ajt" onClick={this.addTelefn.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    {this.posteInit()}        
                       <div type="button" class="ajt" onClick={this.addPostes.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    {this.Maillt()}        
                       <div type="button" class="ajt" onClick={this.addEmailt.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Activité</label>
                    <input required type="text" class="form-control" value={this.state.prospectById.activite} onChange={this.handleChange.bind(this, "activite")} placeholder="insérez l'activité de la société"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Adresse Postale Société</label>
                    <input required type="text" class="form-control" value={this.state.prospectById.adresse_postale_societe} onChange={this.handleChange.bind(this, "adresse_postale_societe")} placeholder="insérez l'adresse postale de la société"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input required type="text" class="form-control" value={this.state.prospectById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateProspect.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter un Prospect </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input type="text" class="form-control" value={this.state.prospectById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le société du prospect"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                    <input  type="text" class="form-control" value={this.state.prospectById.interlocuteur}
                     onChange={this.handleChange.bind(this, "interlocuteur")} 
                     placeholder="insérez le nom du prospect"></input>

                       {this.createUI()}        
                       <div type="button" class="ajt" onClick={this.addClick.bind(this)}><i class="fa fa-plus pls"></i></div>
                 </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label> 
                    <input type="text" class="form-control" value={this.state.prospectById.tel} onChange={this.handleChange.bind(this, "tel")} placeholder="insérez le telephone du prospect"></input>
                    {this.Telephn()}        
                     <div type="button" class="ajt" onClick={this.addTelefn.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    <input type="text" class="form-control" value={this.state.prospectById.poste} onChange={this.handleChange.bind(this, "poste")} placeholder="insérez le poste du prospect"></input>
                    {this.posteInit()}        
                     <div type="button" class="ajt" onClick={this.addPostes.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.prospectById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez l'email du prospect"></input>
                    {this.Maillt()}        
                    <div type="button" class="ajt" onClick={this.addEmailt.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Activité</label>
                    <input type="text" class="form-control" value={this.state.prospectById.activite} onChange={this.handleChange.bind(this, "activite")} placeholder="insérez l'activité de la société"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Adresse Postale Société</label>
                    <input type="text" class="form-control" value={this.state.prospectById.adresse_postale_societe} onChange={this.handleChange.bind(this, "adresse_postale_societe")} placeholder="insérez l'adresse postale de la société"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input type="text" class="form-control" value={this.state.prospectById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateProspect.bind(this)} data-dismiss="modal">Ajouter le prospect</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion prospect</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer le prospect : {this.state.prospectById.societe}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteProspect.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      </div>

    );
  }
}  
export default prospect;