import React from 'react';
import {Bar} from 'react-chartjs-2';
import NavigationBar from './NavigationVar';
import axios from 'axios';
import { parseJwt } from '../util/JWParser';
import cra_data from '../data/CRA_data.json';



export default class gestion_conges extends React.Component {
    constructor (){
      super();
      const token = sessionStorage.getItem("token");
      const decoded = parseJwt(token);
      var sysDate = new Date();
      var annee = sysDate.getFullYear();
      var mois = sysDate.getMonth()+1;
      let now = new Date();
      var month = now.getMonth();
      var current_month = cra_data[month].month
      var jour = sysDate.getDate();
      if (mois>9)
      {
        var today = annee+"-"+mois+"-"+jour
      }
      else if (mois <9)
      {
        var today = annee+"-0"+mois+"-"+jour
      }
       
      this.state= {
        users:[],
        option_month:[],
        option_year:[],
        affectation:[],
        current_month: current_month,
        selectedMonth:'',
        selectedYear:'',
        Month_converted_front:[],
        dayOfWork:[],
        weekdays:[],
        month:'',
        dayAbsent:[],
        decoded:decoded,
        up_conges:{},
        today:today,
        prn:[],
        nm:[],
        all_conges:[],
        demd:[],
        ol_cong:[],
        conge_by_Id:{},
        monthActuel:'',
        delete_index:'',
        query: "",
        data: [],
        filteredData: [],
          }

            this.getConges=this.getConges.bind(this);
            this.handleChangeMonth=this.handleChangeMonth.bind(this);
            this.handleChangeYear=this.handleChangeYear.bind(this);
      }

      OptionsSelect() {       
        var current_month_index = new Date().getMonth();
        for (var i = 0; i < 100; i++) {
            if (i < 12) {
                this.state.month=i;
                this.state.option_month.push(
                    <option value={this.state.month+1}>{cra_data[i].month}</option>
                )
            }
            this.state.option_year.push(<option slected='true' value={parseInt(cra_data[i].year) + i}>{parseInt(cra_data[i].year) + i}</option>)
        }
    // this.setState({option_month:this.state.option_month})
      }

       getConges(){ 
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/user")
          .then(response => response.data)
          .then((data) => {
              this.setState({users:data})
          })
        this.state.month='';
     //   this.setState({all_conges:this.state.all_conges})
        var today=new Date();
        var i = 0;
        var format_date= this.formatDate(today);
         axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
         axios.get("http://45.9.188.225:8080/cra/Userconges/" + this.state.decoded.id)
         axios.get("http://45.9.188.225:8080/cra/AllConges/")
           .then(response => response.data)
           .then((data) => {
            
             this.setState({all_conges:data})

           this.state.all_conges.forEach(conge =>{
                 conge.duree=this.getBusinessDatesCount(new Date(conge.date_debut),new Date(conge.date_fin))-1+" jours"
                 if(conge.date_debut == conge.date_fin){
                   conge.duree = 0.5 + " jour";
                 }
                                               
                })
         this.state.ol_cong=this.state.all_conges;
        
         const { query } = this.state;console.log(query)
         const filteredData = data.filter(element => {
           return ((element.user.prenom.toLowerCase().includes(query.toLowerCase()))
           ||(element.user.nom.toLowerCase().includes(query.toLowerCase())));
         });
         this.setState({ data, filteredData});
         
         this.setState({all_conges:this.state.all_conges})
         console.log(this.state.ol_cong)
        })
        }
        
      getUser(){ 
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        //axios.get("http://45.9.188.225:8080/cra/Userconges/" + this.state.decoded.id)
        axios.get("http://45.9.188.225:8080/cra/user/")
          .then(response => response.data)
          .then((data) => {
            console.log(data)
            data.forEach(user=>
                {
                  console.log(data)
                  user.conges.forEach(conge=>{
                    conge.duree=this.getBusinessDatesCount(new Date(conge.date_debut),new Date(conge.date_fin))-1+" jours"
                    conge.demandeur = user.prenom + " "+ user.nom
                    const { query } = this.state;
                    const filteredData = data.filter(element => {
                      return ((element.nom.toLowerCase().includes(query.toLowerCase()))) ;
                    });
                    this.setState({ data, filteredData});
                    this.state.all_conges.push(conge)
                    console.log(this.state.all_conges)
                  })
              //  this.setState({all_conges:this.state.all_conges})
                })
         
          })
      }

      handleInputChange = event => {
        const query = event.target.value;console.log(query)
        this.setState(prevState => {
          const filteredData = prevState.data.filter(element => {
            return ((element.user.prenom.toLowerCase().includes(query.toLowerCase()))
            ||(element.user.nom.toLowerCase().includes(query.toLowerCase()))) ;
                  //.toLowerCase().includes(event.target.value.toLowerCase())
            });     
            return { query, filteredData };  
        });
      };

      UpdateConge(){
        console.log(this.state.up_conges)
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.post("http://45.9.188.225:8080/cra/updateconges",this.state.up_conges)
           .then(response => response.data)
           .then((data) => {
             data.duree=
             this.getBusinessDatesCount
             (new Date(this.state.all_conges[this.state.delete_index].date_debut),new Date(this.state.all_conges[this.state.delete_index].date_fin))-1+" jours"
            this.state.all_conges[this.state.delete_index]=data
            console.log(this.state.delete_index)
            console.log(this.state.all_conges[this.state.delete_index])
            this.setState({all_conges:this.state.all_conges});
            }).catch((error) => { console.log(error) });
      }
      convert_to_front(date,backDate) {
        
        var t = new Date(date);
        const localOffset2 = t.getTimezoneOffset() * 60000;
        var timestamp2 = (new Date(backDate).getTime() - localOffset2);
        var date = new Date(timestamp2); 
        return date;

    }

      handleChangeMonth(event){
        this.setState({all_conges:this.state.all_conges})
        this.state.selectedMonth=event.target.value;
        if(this.state.selectedMonth.length==1){
           this.state.monthActuel=0+this.state.selectedMonth
        }else{
          this.state.monthActuel=this.state.selectedMonth
        }
        this.setState({selectedMonth:this.selectedMonth})
        var db = this.state.selectedYear+"-"+this.state.monthActuel+"-01";
        var fi = this.state.selectedYear+"-"+this.state.monthActuel+"-31";
        var wd=this.WorkingDays(this.state.selectedMonth,this.state.selectedYear)
        console.log("ye",this.state.selectedYear+"-"+this.state.monthActuel+"-01")
        var dev=[];
        var i=0;
        for( i=0; i<this.state.all_conges.length; i++){
        this.setState({all_conges:this.state.all_conges});
          if(this.state.all_conges[i].date_debut>=db){
            if(this.state.all_conges[i].date_debut<=fi){
              //console.log("true")
              dev.push(this.state.all_conges[i])    
          } }
        }         
         this.setState({ol_cong: dev}
          //,()=> { console.log("fin",this.state.all_conges); }
          );
      }

      handleChangeYear(event){
        // console.log(event.target.value)
        this.setState({all_conges:this.state.all_conges})
        this.state.selectedYear=event.target.value;
        
        this.setState({selectedYear:this.state.selectedYear})
        var db = this.state.selectedYear+"-"+this.state.monthActuel+"-01";
        var fi = this.state.selectedYear+"-"+this.state.monthActuel+"-31";
        var wd=this.WorkingDays(this.state.selectedMonth,this.state.selectedYear)
      console.log("bb",this.state.selectedYear+"-"+this.state.monthActuel+"-01")
        var dev=[];
        var i=0;
        for( i=0; i<this.state.all_conges.length; i++){
        this.setState({all_conges:this.state.all_conges});
          if(this.state.all_conges[i].date_debut>=db){
            if(this.state.all_conges[i].date_debut<=fi){
              
              dev.push(this.state.all_conges[i])    
          } }
        }         
         this.setState({ol_cong: dev});
      }


formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
}

      getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
               count++;
            curDate.setDate(curDate.getDate() + 1);
        }
       // console.log("date", current_month)
        return count;
        }

      componentDidMount(){
     //   this.OptionsSelect();
        this.getConges()
       // this.getUser()
      }

      handleSort(idconges,index,label){
        console.log(idconges)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
          axios.get("http://45.9.188.225:8080/cra/conges/" + idconges )
            .then(response => response.data)
            .then((data) => {
                this.setState({up_conges:data})
                
             if (label=="Rejeté")
             { this.state.up_conges.etat_demande="Rejeté"
             this.state.up_conges.duree=
             this.getBusinessDatesCount(new Date(this.state.up_conges.date_debut),new Date(this.state.up_conges.date_fin))-1+" jours"
            }
             else if (label=="Validé")
             { this.state.up_conges.etat_demande="Validé"
             this.state.up_conges.duree=
             this.getBusinessDatesCount(new Date(this.state.up_conges.date_debut),new Date(this.state.up_conges.date_fin))-1+" jours"
             }
             console.log(this.state.up_conges)
             this.UpdateConge()
            })
            this.state.delete_index=index
            this.setState({delete_index:this.state.delete_index})
      }

      WorkingDays(month, year) {
          var days = 32 - new Date(year, month, 32).getDate();
          var weekdays = 0;
  
          for (var i = 0; i < days; i++) {
              var day = new Date(year, month, i + 1).getDay();
              if (day != 0 && day != 6) weekdays++;
          }
        //  console.log("ab", year, month, day);
          
          this.setState({weekdays});
          return weekdays;
        
      }
       
  render() {
    return (
        <div class="backtotal">
          <NavigationBar/>
            <div className="container conges_container">
            <div className="row view-cra">
                <div className="col-1"></div>
                <div className="col-3">Veuillez selectionner une année</div>
                    <div className="col-3">Veuillez selectionner un mois</div>
                    <div className="col-1"></div>
            </div>
            <div className="row" onLoad={this.OptionsSelect()}>
                <div className="col-1"></div>
                <div className="col-3">
                        <select className="form-control" value={this.state.selectedYear} onChange={this.handleChangeYear.bind(this)} >
                            <option  value=""></option>
                            {this?.state.option_year}
                        </select></div>
                    <div className="col-3">
                        <select className="form-control" value={this.state.selectedMonth} onChange={this.handleChangeMonth.bind(this)} >
                            <option  value=""></option>
                            {this?.state.option_month}
                        </select></div>             
                    <div className="col-1"></div>
                </div>


                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">
                    <div className="searchForm lablef">
            <form>
            <div className="soci"><label>Rechercher par Demandeur : </label> 
             <input class="tstbor" placeholder="Search for..." value={this.state.query} onChange={this.handleInputChange} />
            </div>
            </form>
          </div>
                        <table className="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col" hidden={true}>#</th>
                                <th scope="col"><i class="fas fa-user"></i> Demandeur</th>
                                <th scope="col"><i class="fas fa-spell-check"></i> Motif</th>
                                <th scope="col"><i class="fas fa-calendar-alt"></i> date début</th>
                                <th scope="col"><i class="fas fa-calendar-alt"></i> date de reprise</th>
                                <th scope="col"><i class="fas fa-info-circle"></i> status</th>
                                <th scope="col"><i class="fas fa-stopwatch"></i> durée</th>
                                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
                            </tr>
                            </thead>
                            <tbody>  
                            {this.state.filteredData.map((c,index) => (
                                
                                <tr>
                                
                                <td hidden={true}> {c.idconge}</td>
                                <td>{c.user.prenom} {c.user.nom}</td>
                                <td> {c.motif}</td>
                                <td>{c.date_debut}  </td>
                                <td>{c.date_fin}</td>
                                {c.etat_demande =="En cours"?
                                    <td className="btn btn-warning status"><i class="fas fa-hourglass-half"></i> {c.etat_demande}</td>
                                :c.etat_demande =="Validé"?
                                    <td className="btn btn-success status"><i class="fas fa-hourglass-half"></i> {c.etat_demande}</td>:
                                c.etat_demande =="Rejeté"?
                                    <td className="btn btn-danger status"><i class="fas fa-ban"></i> {c.etat_demande}</td>:<td>-</td>}
                                    <td>{c.duree}</td>
                                <td>
                                    
                                    <div class="btn-group">
                                    <button className="btn btn-success" data-toggle="modal"  onClick={() => this.handleSort(c.idconge,index,"Validé")} alt="valider cette demande" data-target="#exampleModalEdit"><i class="fas fa-check-circle"></i></button>
                                    <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(c.idconge,index,"Rejeté")} data-target="#exampleModalDelete"><i class="fas fa-times-circle"></i></button>
                                    </div>
                                </td>
                                </tr>))}
                            </tbody>
                        </table>
            </div>
            <div className="col-1"></div>
            </div>
            </div>

        </div>
    
    )}
}