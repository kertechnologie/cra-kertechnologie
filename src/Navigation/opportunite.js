import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'

class opportunite extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      opportunites:[],
      opportuniteById:{},
      index:null,
      values: [],
      consults:[],
      custm:[],
      mails : [],
      tlfn : [],
      clients:[],
      postls:[],
      query: "",
      data: [],
      tlfnop:[],
      mlemail:[],
      socie:[],
      alfr:[],
      somcli:[],
      mmp:"",
      eemil:"",
      ttl:"",
      users:[],
      dv : [],
      intrl:[],
      sam : ''
    };
  }

  handleChange(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case 'client': {
            this.state.opportuniteById.client = e.target.value;
            this.setState({ opportuniteById: this.state.opportuniteById });

            for(var i=0; i<this.state.clients.length; i++){
              for(var j=0; j<this.state.socie.length; j++){
                if(this.state.clients[i].societe == this.state.opportuniteById.client){
                    this.state.mmp = this.state.clients[i].interlocuteur
                    this.state.eemil = this.state.clients[i].email
                    this.state.ttl = this.state.clients[i].tel
                    var ofl= this.state.clients[i].societe
                    console.log(this.state.mmp)
                } else if(this.state.socie[j].societe == this.state.opportuniteById.client){
                  this.state.mmp = this.state.socie[j].interlocuteur
                  this.state.eemil = this.state.socie[j].email
                  this.state.ttl = this.state.socie[j].tel
                  var ofl= this.state.socie[j].societe
                  console.log(this.state.mmp)
              }
              }
            }
            if(ofl == this.state.opportuniteById.client){
              this.state.intrl = this.state.mmp.split(' - ')
              this.state.mlemail = this.state.eemil.split(' - ')
              this.state.tlfnop = this.state.ttl.split(' - ')
            }
            console.log(this.state.intrl)
          }
          break;
          case 'commentaire': {
            this.state.opportuniteById.commentaire = e.target.value;
            this.setState({ opportuniteById: this.state.opportuniteById });
          }
          
          break; 
          case 'commerciale': {
            this.state.opportuniteById.commerciale = e.target.value;
            this.setState({ opportuniteById: this.state.opportuniteById });
          }
          
          break; 
          case 'apporteur_affaire': {
            this.state.opportuniteById.apporteur_affaire = e.target.value;
            this.setState({ opportuniteById: this.state.opportuniteById });
          }      
          break;  
          default: 
          break;
        }   
      }
    }
  }

  createUI(){
    return this.state.intrl.map((item, i) =>    
        item.split(' - ').map(item => {
            return (<input disabled={true} type="text" class="form-control" value={item || ''}
             onChange={this.handleChange?.bind(this, i)} />)
        }) /* <label for="exampleFormControlSelect1">Interlocuteur</label> */
    )
  }

  Maillt(){
    return this.state.mlemail.map((item, i) =>    
    item.split(' - ').map(item => {
        return (<input disabled={true} type="text" class="form-control" value={item || ''}
         onChange={this.handleChange?.bind(this, i)} />)
    }) /* <label for="exampleFormControlSelect1">Email</label> */
)
  }

  Telephn(){
    return this.state.tlfnop.map((item, i) =>    
    item.split(' - ').map(item => {
        return (<input disabled={true} type="text" class="form-control" value={item || ''}
         onChange={this.handleChange?.bind(this, i)} />)
    }) /* <label for="exampleFormControlSelect1">Téléphone</label> */
)
  }

  componentDidMount() {    
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        for(var i=0; i<data.length; i++){
          this.setState({users:data})   
           if((this.state.users[i].role == "Commercial")||(this.state.users[i].role == "Admin")){
              this.state.dv[i] = this.state.users[i].prenom + " " + this.state.users[i].nom
           } 
        }
        console.log(this.state.dv)
      })   
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllOpportunites")
      .then(response => response.data)
      .then((data) => {
        const { query } = this.state;
        this.setState({ opportunites: data });  
      })
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllClients")
      .then(response => response.data)
      .then((data) => {
        const { query } = this.state;
          this.setState({ socie: data });  
      })
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllProspects")
      .then(response => response.data)
      .then((data) => {
        const { query } = this.state;
        this.setState({ clients: data });  
      });
  }

  componentWillMount() {
    this.componentDidMount();
  }
  
  componentWillMount() {
    this.componentDidMount();
  }

  AddOpportunite(){
    this.state.opportuniteById.client = '';
    this.state.opportuniteById.interlocuteur = '';
    this.state.opportuniteById.tel = '';
    this.state.opportuniteById.email = '';
    this.state.opportuniteById.commentaire = '';
    this.state.opportuniteById.commerciale = '';
    this.state.opportuniteById.apporteur_affaire = '';
    this.setState({ opportuniteById: this.state.opportuniteById })
  } 

  // ajouter un Opportunite
  ValidateOpportunite() { 
    this.state.opportuniteById.interlocuteur = this.state.intrl.join(' - ')
    this.state.opportuniteById.email = this.state.mlemail.join(' - ')
    this.state.opportuniteById.tel = this.state.tlfnop.join(' - ')

    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/aOpportunite", this.state.opportuniteById) 
      .then(response => response.data)
      .then((opportuniteById) => {
        this.state.opportunites.push(opportuniteById);       
        this.setState({users:this.state.opportunites});
       // window.location.reload(false);
      }).catch((error) => { console.log(error) 
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        un probléme detetcté lors de l'insertion de votre nouveau Opportunite</p>
        this.setState({message:this.state.message})
      });
  }

  //supprimer un Opportunite
  DeleteOpportunite(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/deleteOpportunite/"+this.state.opportuniteById.idopportunite)
      .then(response => response.data)
      .then((data) => {
        this.state.opportunites.splice(this.state.index,1);
        this.setState({users:this.state.opportunites});
      }).catch((error) => { console.log(error) });
  }

  //lire un Opportunite ( pas de tout les opportunites)
  handleSort(idopportunite,index) {
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/Opportunite/" + idopportunite)
      .then(response => response.data)
      .then((data) => {
        this.state.intrl = data.interlocuteur.split(' - ')
        this.state.mlemail = data.email.split(' - ')
        this.state.tlfnop = data.tel.split(' - ')
        console.log(this.state.values)
        this.setState({ opportuniteById: data });
        console.log(this.state.opportuniteById.interlocuteur)
        // this.state.opportuniteById.interlocuteur = this.state.opportuniteById.interlocuteur.split(' - ')
        this.state.index=index ;            
      });
  }

  //modifier un Opportunite
  UpdateOpportunite(){
    this.state.opportuniteById.interlocuteur = this.state.intrl.join(' - ')
    this.state.opportuniteById.email = this.state.mlemail.join(' - ')
    this.state.opportuniteById.tel = this.state.tlfnop.join(' - ')
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/updateOpportunite",this.state.opportuniteById)
      .then(response => response.data)
      .then((data) => {
        this.state.opportunites.splice(this.state.index,1);
        this.state.opportunites.push(data);
        this.setState({opportunites:this.state.opportunites});
        window.location.reload(false);
      }).catch((error) => { console.log(error)
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        votre modification est echouée</p>
        this.setState({message:this.state.message}) 
      });
  }

  render() {
    return (
      <div class="backtotal">
       <NavigationBar/>
       
        <div className="container">
            
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddOpportunite()}>
              <i className="fas fa-plus-circle"></i> Ajouter une nouvelle Opportunite
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
          <table className="table table-sm" >
            <thead>
              <tr>
                <th scope="col" hidden={true}></th>
                <th scope="col"><i class="fas fa-industry"></i> Société </th>
                <th scope="col"><i class="fas fa-user"></i> Interlocuteur</th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Telephone </th>
                <th scope="col"><i class="fas fa-envelope"></i> Email</th>
                <th scope="col"><i class="fas fa-comments"></i> Descriptif</th>
                <th scope="col"><i class="fas fa-user"></i> Commerciale</th>
                <th scope="col"><i class="fas fa-comments"></i> Apporteur d'affaires</th>
                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody> 
             {this.state.opportunites.map((opportunite,index) => (
                <tr >
                  <td hidden={true}>{opportunite.idopportunite}</td>
                  <td>{opportunite.client} </td>
                  <td>{ opportunite.interlocuteur.split(' - ').map(item => { return (<div> {item} </div>)})}</td> 
                  <td>{opportunite.tel.split(' - ').map(item => {return (<div> {item} </div>)})}</td>
                  <td>{opportunite.email.split(' - ').map(item => {return (<div> {item} </div>)})}</td>
                  <td> {opportunite.commentaire.length>=20? 
                          <div key={opportunite.commentaire.length}>{opportunite.commentaire.substring(0, 20) + ' ...'}</div> 
                        : <div>{opportunite.commentaire} </div>
                       }
                  </td>
                  <td>{opportunite.commerciale}</td>
                  <td> {opportunite.apporteur_affaire.length>=20? 
                          <div key={opportunite.apporteur_affaire.length}>{opportunite.apporteur_affaire.substring(0, 20) + ' ...'}</div> 
                        : <div>{opportunite.apporteur_affaire} </div>
                       }
                  </td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(opportunite.idopportunite,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(opportunite.idopportunite,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(opportunite.idopportunite,index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(opportunite.idopportunite,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données d'opportunite: {this.state.opportuniteById.client} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-address-card"></i> Société</td>
                      <td>{this.state.opportuniteById.client}</td>
                    </tr>

                    <tr>
                      <td><i class="fas fa-user"></i> Interlocuteur </td>
                     <td> 
                        { this.state.intrl.map((item) => (     
                          item.split(' - ').map(item => { return (<div> {item} </div>)})))
                        }
                     </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Telephone </td>
                      <td>{ this.state.tlfnop.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))}
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Email </td>
                      <td>
                      { this.state.mlemail.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))
                        }
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-comments"></i> Descriptif</td>
                      <td>{this.state.opportuniteById.commentaire}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Commerciale</td>
                      <td>{this.state.opportuniteById.commerciale}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-comments"></i> Apporteur d'affaires</td>
                      <td>{this.state.opportuniteById.apporteur_affaire}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de l'opportunité pour le prospect: {this.state.opportuniteById.client} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <select required class="form-control" value={this.state.opportuniteById.client} onChange={this.handleChange.bind(this, "client")}>
                      <option value=""> Select one...</option>
                        {this.state.clients.map(msgTemplate => (
                        <option key={msgTemplate.id} value={msgTemplate.societe}>
                           {msgTemplate.societe}
                        </option>))}
                        {this.state.socie.map(msgTemplate => (
                        <option key={msgTemplate.id} value={msgTemplate.societe}>
                           {msgTemplate.societe}
                        </option>))}
                    </select>     
                  </div>
                    <div class="alin col-md-12">
                  <div class="form-group col-md-4">
                      <div> {this.createUI()} </div>      
                 </div>
                  <div class="form-group col-md-4">
                      <div>{this.Telephn()} </div>     
                  </div>
                  <div class="form-group col-md-4">
                      <div>{this.Maillt()}</div>      
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Descriptif</label>
                    <input required type="text" class="form-control" value={this.state.opportuniteById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un descriptif"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commerciale</label>
                    <select required class="form-control" value={this.state.opportuniteById.commerciale} onChange={this.handleChange.bind(this, "commerciale")}>
                      <option value="">Select one...</option>
                      {this.state.dv.map(msgTemplate => (
                        <option key={msgTemplate.id}>
                          {msgTemplate}
                        </option>))}
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Apporteur d'affaires</label>
                    <input required type="text" class="form-control" value={this.state.opportuniteById.apporteur_affaire} onChange={this.handleChange.bind(this, "apporteur_affaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateOpportunite.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
   
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter une Opportunite </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <select required class="form-control" value={this.state.opportuniteById.client} onChange={this.handleChange.bind(this, "client")}>
                      <option value=""> Select one...</option>
                        {this.state.clients.map(msgTemplate => (
                        <option key={msgTemplate.id} value={msgTemplate.societe}>
                           {msgTemplate.societe}
                        </option>))}
                        {this.state.socie.map(msgTemplate => (
                        <option key={msgTemplate.id} value={msgTemplate.societe}>
                           {msgTemplate.societe}
                        </option>))}
                    </select>
                  </div>
                  <div class="alin col-md-12">
                  <div class="form-group col-md-4">
                    
                      <div> {this.createUI()}</div>      
                 </div>
                  <div class="form-group col-md-4">
                      <div>{this.Telephn()} </div>     
                  </div>
                  <div class="form-group col-md-4">
                      <div>{this.Maillt()}</div>      
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Descriptif</label>
                    <input type="text" class="form-control" value={this.state.opportuniteById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un descriptif"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commerciale</label>
                    <select required class="form-control" value={this.state.opportuniteById.commerciale} onChange={this.handleChange.bind(this, "commerciale")}>
                      <option value="">Select one...</option>
                      {this.state.dv.map(msgTemplate => (
                        <option key={msgTemplate.id}>
                          {msgTemplate}
                        </option>))}
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Apporteur d'affaires</label>
                    <input type="text" class="form-control" value={this.state.opportuniteById.apporteur_affaire} onChange={this.handleChange.bind(this, "apporteur_affaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateOpportunite.bind(this)} data-dismiss="modal">Ajouter l'pportunite</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion Opportunite</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer l'opportunité : {this.state.opportuniteById.client}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteOpportunite.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      </div>

    );
  }
}  
export default opportunite;