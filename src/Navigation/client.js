import React from 'react';
import '../App.css';
import axios from 'axios';
import { withRouter } from 'react-router'

import NavigationBar from './NavigationVar'

class client extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clients:[],
      clientById:{},
      index:null,
      values: [],
      consults:[],
      users:[],
      mails : [],
      tlfn : [],
      postls:[],
      query: "",
      data: [],
      filteredData: [],
      dv : []  
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChanPost = this.handleChanPost.bind(this);
    this.handleChanEmail = this.handleChanEmail.bind(this);
    this.handleChanTlfn = this.handleChanTlfn.bind(this);
    this.handleChngs = this.handleChngs.bind(this);
    this.handleInterlo = this.handleInterlo.bind(this);
  }

  handleChange(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case 'societe': {
            this.state.clientById.societe = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;
          case 'activite': {
            this.state.clientById.activite = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break; 
          case 'date_debut': {
            this.state.clientById.date_debut = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;  
          case 'date_fin': {
            this.state.clientById.date_fin = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;   
          case 'consultant': {
            this.state.clientById.consultant = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;
          case 'tel': {
            this.state.clientById.tel = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;
          case 'poste': {
            this.state.clientById.poste = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;
          case 'email': {
            this.state.clientById.email = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;
          case 'adresse_postale_societe': {
            this.state.clientById.adresse_postale_societe = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break;
          case 'interlocuteur': {
            this.state.clientById.interlocuteur = e.target.value;
            this.setState({ clientById: this.state.clientById });
          }
          break; 
          default: 
          break;
        }   
      }
    }
  }

  handleChngs(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let consults = [...this.state.consults];
            consults[i] = e.target.value;
            this.setState({  consults  });
          }
        break;  
        default:
        break;
        }
      }
    }
  }

  handleInterlo(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let values = [...this.state.values];
            values[i] = e.target.value;
            this.setState({  values  });
          }
        break;  
        default:
        break;
        }
      }
    }
  }

  handleChanEmail(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let mails = [...this.state.mails];
            mails[i] = e.target.value;
            this.setState({  mails  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  handleChanPost(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let postls = [...this.state.postls];
            postls[i] = e.target.value;
            this.setState({  postls  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }

  createUI(){
    return this.state.values?.map((el, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre nom du client" value={el||''} class="form-control" onChange={this.handleInterlo.bind(this, i)} />       
        <button value='remove' class="detr" onClick={this.removeClick.bind(this, i)}>
        <i class="fa fa-minus sam"></i></button>           
      </div>   
    )
  }

  addClick(){
    this.setState(prevState => ({ values: [...prevState.values, '']}))
    console.log("cc", this.state.values)
  }
  
  removeClick(i){
     let values = [...this.state.values];
     values.splice(i,1);
     this.setState({ values });
  }

  cnslt(){
    return this.state.consults?.map((item, i) => 
      <div class="rd" key={i}> 
        
          <select  class="form-control" onChange={this.handleChngs.bind(this, i)} value={item||''}>
                      <option value="">Select one... </option>
                      {this.state.dv.map(msgTemplate => (
                        <option key={msgTemplate.id}>
                          {msgTemplate}
                        </option>))}
                    </select>
        <button value='remove' class="detr" onClick={this.removeConsult.bind(this, i)}>
        <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }
 
  addConsulat(){
    this.setState(prevState => ({ consults: [...prevState.consults, '']}))
    console.log("cc", this.state.consults)
  } 

  removeConsult(i){
    let consults = [...this.state.consults];
    consults.splice(i,1);
    this.setState({ consults });
  }

  Maillt(){
    return this.state.mails.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre email du client" value={item||''} class="form-control"
          onChange={this.handleChanEmail.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removeEmail.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addEmailt(){
    this.setState(prevState => ({ mails: [...prevState.mails, '']}))
    console.log("cc", this.state.mails)
  }

  removeEmail(i){
    let mails = [...this.state.mails];
    mails.splice(i,1);
    this.setState({ mails });
  }

   handleChanTlfn(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case (i) :{
            let tlfn = [...this.state.tlfn];
            tlfn[i] = e.target.value;
            this.setState({  tlfn  });
          }
          break;  
          default:
          break;
        }
      }
    }
  }
  Telephn(){
    return this.state.tlfn.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre numéro du téléphone" value={item||''} class="form-control"
          onChange={this.handleChanTlfn.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removeTelefn.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addTelefn(){
    this.setState(prevState => ({ tlfn: [...prevState.tlfn, '']}))
    console.log("cc", this.state.tlfn)
  }

  removeTelefn(i){
    let tlfn = [...this.state.tlfn];
    tlfn.splice(i,1);
    this.setState({ tlfn });
  }

  posteInit(){
    return this.state.postls.map((item, i) => 
      <div class="rd" key={i}> 
        <input type="text" placeholder="insérez un autre poste du client" value={item||''} class="form-control"
          onChange={this.handleChanPost.bind(this, i)} />
        <button value='remove' class="detr" onClick={this.removePostes.bind(this, i)}>
           <i class="fa fa-minus sam"></i></button>         
      </div>   
    )
  }

  addPostes(){
    this.setState(prevState => ({ postls: [...prevState.postls, '']}))
    console.log("cc", this.state.postls)
  }

  removePostes(i){
    let postls = [...this.state.postls];
    postls.splice(i,1);
    this.setState({ postls });
  }

  // lire tout les clients
  componentDidMount() {    
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        for(var i=0; i<data.length; i++){
          this.setState({users:data})   
           if((this.state.users[i].role == "Utilisateur")&&(this.state.users[i].statut == "Activé")){
              this.state.dv[i] = this.state.users[i].prenom + " " + this.state.users[i].nom
           } 
        }
      })   
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllClients")
      .then(response => response.data)
      .then((data) => {
        const { query } = this.state;
        const filteredData = data.filter(element => {
          return ((element.societe.toLowerCase().includes(query.toLowerCase())) 
              ||(element.poste.toLowerCase().includes(query.toLowerCase()))
              ||(element.interlocuteur.toLowerCase().includes(query.toLowerCase()))) ;
        });
        this.setState({ data, filteredData});
        this.setState({ clients: data });  
      });
  }

  componentWillMount() {
    this.componentDidMount();
  }

  //pour faire un filtrage par societe, interlocuteur ou poste
  handleInputChange = event => {
    const query = event.target.value;
    this.setState(prevState => {
      const filteredData = prevState.data.filter(element => {
        return ((element.societe.toLowerCase().includes(query.toLowerCase())) 
              ||(element.poste.toLowerCase().includes(query.toLowerCase()))
              ||(element.interlocuteur.toLowerCase().includes(query.toLowerCase()))) ;
              //.toLowerCase().includes(event.target.value.toLowerCase())
        }); 
        console.log("bb",filteredData.poste)

        return { query, filteredData };  
    });
  };
  
  componentWillMount() {
    this.componentDidMount();
  }

  AddClient(){
    this.state.clientById.societe = '';
    this.state.clientById.interlocuteur = '';
    this.state.clientById.consultant = '';
    this.state.clientById.tel = '';
    this.state.clientById.poste = '';
    this.state.clientById.email = '';
    this.state.clientById.adresse_postale_societe = '';
    this.state.clientById.activite = '';
    this.state.clientById.date_debut = '';
    this.state.clientById.date_fin = '';
    this.setState({ clientById: this.state.clientById })
  } 

  //Ajouter un client
  ValidateClient() { 
    var nom =this.state.values.join(' - ')
    var consultants = this.state.consults.join(' - ')
    var ymail = this.state.mails.join(' - ')
    var tlifn = this.state.tlfn.join(' - ')
    var pstl = this.state.postls.join(' - ')
    console.log(nom.length);
    if(nom.length==0){
      this.state.clientById.interlocuteur = this.state.clientById.interlocuteur
    }else{
      this.state.clientById.interlocuteur = this.state.clientById.interlocuteur + ' - ' + nom
    }
    if(consultants.length==0){
      this.state.clientById.consultant = this.state.clientById.consultant
    }else{
      this.state.clientById.consultant = this.state.clientById.consultant + ' - ' + consultants
    }
    if(ymail.length==0){
      this.state.clientById.email = this.state.clientById.email
    }else{
      this.state.clientById.email = this.state.clientById.email + ' - ' + ymail
    }
    if(tlifn.length==0){
      this.state.clientById.tel = this.state.clientById.tel
    }else{
      this.state.clientById.tel = this.state.clientById.tel + ' - ' + tlifn
    }
    if(pstl.length==0){
      this.state.clientById.poste = this.state.clientById.poste
    }else{
      this.state.clientById.poste = this.state.clientById.poste + ' - ' + pstl
    }

    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/aClient", this.state.clientById) 
      .then(response => response.data)
      .then((clientById) => {
        this.state.clients.push(clientById);  
        this.setState({users:this.state.clients});
        window.location.reload(false);

      }).catch((error) => { console.log(error) 
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        un probléme detetcté lors de l'insertion de votre nouveau client</p>
        this.setState({message:this.state.message})
      });
  }

  //supprimer un client
  DeleteClient(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/deleteClient/"+this.state.clientById.idclient)
      .then(response => response.data)
      .then((data) => {
        this.state.clients.splice(this.state.index,1);
        this.setState({users:this.state.clients});
        window.location.reload(false);
      }).catch((error) => { console.log(error) });
  }

  //lire un client (pas tout les clients)
  handleSort(idclient,index) {
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/client/" + idclient)
      .then(response => response.data)
      .then((data) => {
        this.state.values = data.interlocuteur?.split(' - ')
        console.log(this.state.values);
        this.state.consults = data.consultant?.split(' - ')
        this.state.mails = data.email.split(' - ')
        this.state.tlfn = data.tel.split(' - ')
        this.state.postls = data.poste.split(' - ')
        this.setState({ clientById: data });
        this.state.index=index ;            
      });
  }

  //modifier un client
  UpdateClient(){
    var nom =this.state.values.join(' - ')
    this.state.clientById.interlocuteur = nom
    var colts =this.state.consults.join(' - ')
    this.state.clientById.consultant = colts
    this.state.clientById.email = this.state.mails.join(' - ')
    this.state.clientById.tel = this.state.tlfn.join(' - ')
    this.state.clientById.poste = this.state.postls.join(' - ')
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/updateClient",this.state.clientById)
      .then(response => response.data)
      .then((data) => {
        this.state.clients.splice(this.state.index,1);
        this.state.clients.push(data);
        console.log("dd", this.state.clients)
        this.setState({clients:this.state.clients});
        window.location.reload(false);
      }).catch((error) => { console.log(error)
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        votre modification est echouée</p>
        this.setState({message:this.state.message}) 
      });
  }

  render() {
    return (
      <div class="backtotal">
       <NavigationBar/>
        <div className="container">
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddClient()}>
              <i className="fas fa-plus-circle"></i> Ajouter un nouveau Client
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
          <div className="searchForm rr">
            <form>
            <div className="soci"><label>Rechercher par Société, Nom de l'interlocuteur ou par Poste :</label> 
             <input class="tstbor" placeholder="Search for..." value={this.state.query} onChange={this.handleInputChange} />
            </div>
            </form>
          </div>
          <table className="table table-sm" >
            <thead>
              <tr>
                <th scope="col" hidden={true}></th>
                <th scope="col"><i class="fas fa-industry"></i> Société</th>
                <th scope="col"><i class="fas fa-user"></i> Interlocuteur</th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Telephone </th>
                <th scope="col"><i class="fas fa-briefcase"></i> Poste</th>
                <th scope="col"><i class="fas fa-envelope"></i> Email</th>
                <th scope="col"><i class="fas fa-user"></i> Consultant</th>
                <th scope="col"><i class="fas fa-tasks"></i> Activité</th>
                
                <th scope="col"><i class="fas fa-address-card"></i> Adresse Postale Société</th>
                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody>                
             {this.state.filteredData.map((client,index) => (
                <tr >
                  <td hidden={true}>{client.idclient}</td>
                  <td>{client.societe} </td>
                  <td>{ client.interlocuteur?.split(' - ').map(item => { return (<div> {item} </div>)})}</td> 
                  <td>{client.tel.split(' - ').map(item => {return (<div> {item} </div>)})}</td>
                  <td>{client.poste.split(' - ').map(item => {return (<div> {item} </div>)})}</td>
                  <td>{client.email.split(' - ').map(item => {return (<div> {item} </div>)})}</td>
                  <td>{ client.consultant?.split(' - ').map(item => {return (<div> {item} </div>)})}</td>
                  <td>{client.activite}</td>
                  <td>
                  {client.adresse_postale_societe?.length>=20? 
                          <div key={client.adresse_postale_societe.length}>{client.adresse_postale_societe.substring(0, 20) + ' ...'}</div> 
                        : <div>{client.adresse_postale_societe} </div>}
                  </td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(client.idclient,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(client.idclient,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(client.idclient,index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(client.idclient,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données du client: {this.state.clientById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-address-card"></i> Société</td>
                      <td>{this.state.clientById.societe}</td>
                    </tr>

                    <tr>
                      <td><i class="fas fa-user"></i> Interlocuteur </td>
                     <td> 
                        { this.state.values?.map((item) => (     
                          item.split(' - ').map(item => { return (<div> {item} </div>)})))
                        }
                     </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Telephone </td>
                      <td>{ this.state.tlfn.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))}
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Poste </td>
                      <td>{ this.state.postls.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))}
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Email </td>
                      <td>
                      { this.state.mails.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))
                        }
                      </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-envelope"></i> Consultant </td>
                      <td> 
                        { this.state.consults?.map((clt) => (
                          clt.split(' - ').map(clt => { return (<div> {clt} </div>)})))
                        }
                     </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Activité</td>
                      <td>{this.state.clientById.activite}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Date debut</td>
                      <td>{this.state.clientById.date_debut}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Date fin</td>
                      <td>{this.state.clientById.date_fin}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Adresse Postale Société</td>
                      <td>{this.state.clientById.adresse_postale_societe}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
    
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.clientById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input required type="text" class="form-control" value={this.state.clientById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le société du client"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                       {this.createUI()}        
                       <div type="button" class="ajt" onClick={this.addClick.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label>
                    {this.Telephn()}        
                       <div type="button" class="ajt" onClick={this.addTelefn.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    {this.posteInit()}        
                       <div type="button" class="ajt" onClick={this.addPostes.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    {this.Maillt()}        
                       <div type="button" class="ajt" onClick={this.addEmailt.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Consultant</label>
                    {this.cnslt()}        
                       <div type="button" class="ajt" onClick={this.addConsulat.bind(this)}><i class="fa fa-plus pls"></i></div> 
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Activité</label>
                    <input required type="text" class="form-control" value={this.state.clientById.activite} onChange={this.handleChange.bind(this, "activite")} placeholder="insérez l'activité de la société"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date debut</label>
                    <input required type="date" class="form-control" value={this.state.clientById.date_debut} onChange={this.handleChange.bind(this, "date_debut")} placeholder="insérez le date debut de la mission"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date fin</label>
                    <input required type="date" class="form-control" value={this.state.clientById.date_fin} onChange={this.handleChange.bind(this, "date_fin")} placeholder="insérez le date fin de la mission"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Adresse Postale Société</label>
                    <input required type="text" class="form-control" value={this.state.clientById.adresse_postale_societe} onChange={this.handleChange.bind(this, "adresse_postale_societe")} placeholder="insérez l'adresse postale de la société"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateClient.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter un client </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input type="text" class="form-control" value={this.state.clientById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le société du client"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                    <input  type="text" class="form-control" value={this.state.clientById.interlocuteur}
                     onChange={this.handleChange.bind(this, "interlocuteur")} 
                     placeholder="insérez le nom du client"></input>

                       {this.createUI()}        
                       <div type="button" class="ajt" onClick={this.addClick.bind(this)}><i class="fa fa-plus pls"></i></div>
                 </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label> 
                    <input type="text" class="form-control" value={this.state.clientById.tel} onChange={this.handleChange.bind(this, "tel")} placeholder="insérez le numéro du téléphone du client"></input>
                    {this.Telephn()}        
                     <div type="button" class="ajt" onClick={this.addTelefn.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    <input type="text" class="form-control" value={this.state.clientById.poste} onChange={this.handleChange.bind(this, "poste")} placeholder="insérez le poste du client"></input>
                    {this.posteInit()}        
                     <div type="button" class="ajt" onClick={this.addPostes.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.clientById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez l'email du client"></input>
                    {this.Maillt()}        
                    <div type="button" class="ajt" onClick={this.addEmailt.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Consultant</label>
                    <select required class="form-control" value={this.state.clientById.consultant} onChange={this.handleChange.bind(this, "consultant")}>
                      <option value="">Select one... </option>
                      {this.state.dv.map(msgTemplate => (
                        <option key={msgTemplate.id}>
                          {msgTemplate}
                        </option>))}
                    </select>
                     {this.cnslt()}        
                     <div type="button" class="ajt" onClick={this.addConsulat.bind(this)}><i class="fa fa-plus pls"></i></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Activité</label>
                    <input type="text" class="form-control" value={this.state.clientById.activite} onChange={this.handleChange.bind(this, "activite")} placeholder="insérez l'activité de la société"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date debut</label>
                    <input type="date" class="form-control" value={this.state.clientById.date_debut} onChange={this.handleChange.bind(this, "date_debut")} placeholder="insérez le date debut de la mission"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date fin</label>
                    <input type="date" class="form-control" value={this.state.clientById.date_fin} onChange={this.handleChange.bind(this, "date_fin")} placeholder="insérez le date fin de la mission"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Adresse Postale Société</label>
                    <input type="text" class="form-control" value={this.state.clientById.adresse_postale_societe} onChange={this.handleChange.bind(this, "adresse_postale_societe")} placeholder="insérez l'adresse postale de la société"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateClient.bind(this)} data-dismiss="modal">Ajouter le client</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion client</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer le client : {this.state.clientById.societe}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteClient.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
     
      </div>

    );
  }
}  
export default client;