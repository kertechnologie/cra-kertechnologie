import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'

class consultant extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        consultants:[],
        users:[],
        dv:[],
        aab:[],
        ccd:[],
        eef:[],
        ggh:[],
        consultantById:{},
        index:null,
        style:"",
        query: "",
        data: [],
        filteredData: [],
      };
    }

    handleChange(label, e) {
        if (e) {
          if (e) {
            switch (label) {
              case 'nom': {
                this.state.consultantById.nom = e.target.value;
                this.setState({ consultantById: this.state.consultantById });
                this.state.aab = this.state.consultantById.nom.slice(0,1);
                this.state.ccd = this.state.consultantById.nom.split(' ');
                this.state.eef = this.state.ccd[1].slice(0,2);
                this.state.ggh = (this.state.aab +'.' + this.state.eef).toUpperCase();                 
                console.log(this.state.consultantById.nom)
                console.log(this.state.ggh)
              }
              break;
              case 'ref': {
                this.state.consultantById.ref = e.target.value;
                this.setState({ consultantById: this.state.consultantById });
              }
              break;
              case 'specialite': {
                this.state.consultantById.specialite = e.target.value;
                this.setState({ consultantById: this.state.consultantById });
              }
              break;
              case 'competence': {
                this.state.consultantById.competence = e.target.value;
                this.setState({ consultantById: this.state.consultantById });
              }
              break;
              case 'statut': {
                this.state.consultantById.statut = e.target.value;
                this.setState({ consultantById: this.state.consultantById });
              }
              break;
              case 'client': {
                this.state.consultantById.client = e.target.value;
                this.setState({ consultantById: this.state.consultantById });
              }
              break;
              case 'tjm': {
                this.state.consultantById.tjm = e.target.value;
                this.setState({ consultantById: this.state.consultantById });
              }
              break;
              default: 
              break;
      
          }
    
        }
      }
    }

    componentDidMount() {   
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        for(var i=0; i<data.length; i++){
          this.setState({users:data})   
           if((this.state.users[i].role == "Utilisateur")&&(this.state.users[i].statut == "Activé")){
              this.state.dv[i] = this.state.users[i].prenom + " " + this.state.users[i].nom
           } 
        }
        console.log(this.state.dv)
      })    
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.get("http://45.9.188.225:8080/cra/AllConsultants")
        .then(response => response.data)
        .then((data) => {
          const { query } = this.state;
        const filteredData = data.filter(element => {
          return ((element.statut.toLowerCase().includes(query.toLowerCase()))) ;
        });
        this.setState({ data, filteredData});
          this.setState({ consultants: data });
        });
    }

    handleInputChange = event => {
      const query = event.target.value;
      this.setState(prevState => {
        const filteredData = prevState.data.filter(element => {
          return ((element.statut.toLowerCase().includes(query.toLowerCase()))) ;
                //.toLowerCase().includes(event.target.value.toLowerCase())
          }); 
          console.log("bb",filteredData.statut)
  
          return { query, filteredData };  
      });
    };

    AddConsultant(){
      this.state.consultantById.nom = '';
      this.state.consultantById.ref = '';
      this.state.consultantById.specialite = '';
      this.state.consultantById.competence = '';
      this.state.consultantById.statut = '';
      this.state.consultantById.client = '';
      this.state.consultantById.tjm = '';
      this.setState({ consultantById: this.state.consultantById })
    }

    ValidateConsultant() { 
      this.state.consultantById.ref = this.state.ggh;
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.post("http://45.9.188.225:8080/cra/aConsultant", this.state.consultantById)    
        .then(response => response.data)
        .then((consultantById) => {
          this.state.consultants.push(consultantById);
          this.setState({users:this.state.consultants});
          window.location.reload(false);
        }).catch((error) => { console.log(error) 
          this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
          un probléme detetcté lors de l'insertion de votre nouveau consultant</p>
          this.setState({message:this.state.message})
        });
    }

    DeleteConsultant(){
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.get("http://45.9.188.225:8080/cra/deleteConsultant/"+this.state.consultantById.idconsultant)
        .then(response => response.data)
        .then((data) => {
         this.state.consultants.splice(this.state.index,1);
         this.setState({users:this.state.consultants});
         window.location.reload(false);
         }).catch((error) => { console.log(error) });
   }

    handleSort(idconsultant,index) {
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.get("http://45.9.188.225:8080/cra/consultant/" + idconsultant)
        .then(response => response.data)
        .then((data) => {
          this.state.ggh = data.ref;
          this.setState({ consultantById: data });
          this.state.index=index ;            
        });
    }

    UpdateConsultant(){
      this.state.consultantById.ref = this.state.ggh;
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.post("http://45.9.188.225:8080/cra/updateConsultant",this.state.consultantById)
        .then(response => response.data)
        .then((data) => {
         this.state.consultants.splice(this.state.index,1);
         this.state.consultants.push(data);
         this.setState({consultants:this.state.consultants});
         window.location.reload(false);
        }).catch((error) => { console.log(error)
          this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
          votre modification est echouée</p>
          this.setState({message:this.state.message}) 
       });
   }

   render() {
    return (
      <div class="backtotal">
       <NavigationBar/>
        <div className="container">
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddConsultant()}>
              <i className="fas fa-plus-circle"></i> Ajouter un nouveau consultant
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
          <div className="searchForm rr">
            <form>
            <div className="soci"><label>Rechercher par Statut : </label> 
             <input class="tstbor" placeholder="Search for..." value={this.state.query} onChange={this.handleInputChange} />
            </div>
            </form>
          </div>
          <table className="nopt table table-sm">
            <thead>
              <tr>
                <th scope="col" hidden={true}></th>
                <th scope="col"><i class="fas fa-user"></i> Ref</th>
                <th scope="col"><i class="fas fa-user"></i> Consultant</th>
                <th scope="col"><i class="fas fa-address-card"></i> Profil</th>
                <th scope="col"><i class="fas fa-address-card"></i> Compétence</th>
                <th scope="col"><i class="fas fa-address-card"></i> Statut</th>
                <th scope="col"><i class="fas fa-industry"></i> Client </th>
                <th scope="col"><i class="fas fa-money-check"></i> TJM</th>
                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.filteredData.map((consultant,index) => (
                <tr >
                  <td hidden={true}> {consultant.idconsultant}</td>
                  <td>{consultant.ref}</td>
                  <td>{consultant.nom}</td>
                  <td>{consultant.specialite}</td>
                  <td>{consultant.competence}</td>
                  <td>{consultant.statut}</td>
              <td>{consultant.statut=="En mission" ? <div key={consultant.statut}>{consultant.client}</div>:<div></div>}</td>
                  <td>{consultant.tjm}</td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(consultant.idconsultant,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(consultant.idconsultant,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(consultant.idconsultant,index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(consultant.idconsultant,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données du consultant: {this.state.consultantById.nom} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-user"></i> Ref </td>
                      <td>{this.state.consultantById.ref} </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-user"></i> Consultant </td>
                      <td>{this.state.consultantById.nom} </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Profil </td>
                      <td>{this.state.consultantById.specialite}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Compétence </td>
                      <td>{this.state.consultantById.competence}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Statut</td>
                      <td>{this.state.consultantById.statut}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Client </td>
                      <td>{this.state.consultantById.client}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> TJM </td>
                      <td>{this.state.consultantById.tjm}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
    
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.consultantById.nom} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Consultant</label>
                    <select required class="form-control" value={this.state.consultantById.nom} onChange={this.handleChange.bind(this, "nom")}>
                      <option value="">Select one...</option>
                      {this.state.dv.map(msgTemplate => (
                        <option key={msgTemplate.id}>
                          {msgTemplate}
                        </option>))}
                    </select>              
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Ref</label>
                    <input disabled={true} required class="form-control" value={this.state.ggh} onChange={this.handleChange.bind(this, "ref")} />
                  </div>
                  <label for="exampleFormControlSelect1">Profil</label>
                    <select className="form-control" value={this.state.consultantById.specialite} onChange={this.handleChange.bind(this, "specialite")}>
                      <option value=""></option>
                      <option value="Full stack-Java">Full stack-Java</option>
                      <option value="Full stack-.Net">Full stack-.Net</option>
                      <option value="Full stack-PHP">Full stack-PHP</option>
                      <option value="DEVOPS">DEVOPS</option>
                      <option value="Talend">Talend</option>
                      <option value="MSBI">MSBI</option>
                      <option value="Informatica">Informatica</option>
                      <option value="DataStage">DataStage</option>
                      <option value="BI">BI</option>
                      <option value="MOA">MOA</option>
                      <option value="AMOA">AMOA</option>
                      <option value="Salesforce">Salesforce</option>
                      <option value="Système">Système</option>
                      <option value="Réseau">Réseau</option>
                      <option value="Sécurité">Sécurité</option>
                      <option value="DBA">DBA</option>
                      <option value="CRM">CRM</option>
                    </select>
                    <div class="form-group">
                    <label for="exampleFormControlSelect1">Compétence</label>
                    <input required type="text" class="form-control" value={this.state.consultantById.competence} onChange={this.handleChange.bind(this, "competence")} placeholder="insérez les compétences du consultant"></input>
                    </div>
                    <div class="form-group">
                    <label for="exampleFormControlSelect1">Statut</label>
                    <select className="form-control" value={this.state.consultantById.statut} onChange={this.handleChange.bind(this, "statut")}>
                      <option value=""></option>
                      <option value="En mission">En mission</option>
                      <option value="Intercontrat">Intercontrat</option>
                    </select>
                    </div>
                    <div class="form-group">
                    {this.state.consultantById.statut=="En mission"? 
                          <input key={this.state.consultantById.statut}   
                           type="text" class="form-control" value={this.state.consultantById.client} onChange={this.handleChange.bind(this, "client")} placeholder="insérez un client"/> 
                        : <div disabled={true}> </div>
                       }
                  </div>
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">TJM</label>
                          <input type="text" class="form-control" value={this.state.consultantById.tjm}
                            onChange={this.handleChange.bind(this, "tjm")} placeholder="insérez un tjm"/>                        
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateConsultant.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
      
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter un consultant </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Consultant</label>
                    <select required class="form-control" value={this.state.consultantById.nom} onChange={this.handleChange.bind(this, "nom")}>
                      <option value="">Select one...</option>
                      {this.state.dv.map(msgTemplate => (
                        <option key={msgTemplate.id}>
                          {msgTemplate}
                        </option>))}
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Ref</label>
                    <input disabled={true} required class="form-control" value={this.state.ggh} onChange={this.handleChange.bind(this, "ref")} />
                  </div>
                  
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Profil</label>
                    {/* <input type="text" class="form-control" value={this.state.up_conges.motif}  onChange={this.handleChange.bind(this, "motif")} placeholder="insérez le motif de l'absence"></input> */}
                    <select class="form-control" value={this.state.consultantById.specialite}  onChange={this.handleChange.bind(this, "specialite")}>
                      <option value=""></option>
                      <option value="Full stack-Java">Full stack-Java</option>
                      <option value="Full stack-.Net">Full stack-.Net</option>
                      <option value="Full stack-PHP">Full stack-PHP</option>
                      <option value="DEVOPS">DEVOPS</option>
                      <option value="Talend">Talend</option>
                      <option value="MSBI">MSBI</option>
                      <option value="Informatica">Informatica</option>
                      <option value="DataStage">DataStage</option>
                      <option value="BI">BI</option>
                      <option value="MOA">MOA</option>
                      <option value="AMOA">AMOA</option>
                      <option value="Salesforce">Salesforce</option>
                      <option value="Système">Système</option>
                      <option value="Réseau">Réseau</option>
                      <option value="Sécurité">Sécurité</option>
                      <option value="DBA">DBA</option>
                      <option value="CRM">CRM</option>
                    </select>
                  </div>
                 
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Compétence</label>
                    <input required type="text" class="form-control" value={this.state.consultantById.competence} onChange={this.handleChange.bind(this, "competence")} placeholder="insérez les compétences du consultant"></input>
                    </div>
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Statut</label>
                    {/* <input type="text" class="form-control" value={this.state.up_conges.motif}  onChange={this.handleChange.bind(this, "motif")} placeholder="insérez le motif de l'absence"></input> */}
                    <select class="form-control" value={this.state.consultantById.statut}  onChange={this.handleChange.bind(this, "statut")}>
                      <option value=""></option>
                      <option value="En mission">En mission</option>
                      <option value="Intercontrat">Intercontrat</option>
                    </select>
                  </div>
                  <div class="form-group">
                    {this.state.consultantById.statut=="En mission"? 
                          <input key={this.state.consultantById.statut}   
                           type="text" class="form-control" value={this.state.consultantById.client} onChange={this.handleChange.bind(this, "client")} placeholder="insérez un client"/> 
                        : <div disabled={true}> {this.state.consultantById.client} </div>
                       }
                  </div>
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">TJM</label>
                          <input type="text" class="form-control" value={this.state.consultantById.tjm}
                            onChange={this.handleChange.bind(this, "tjm")} placeholder="insérez un tjm"/>                        
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateConsultant.bind(this)} data-dismiss="modal">Ajouter le consultant</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion consultant</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer le consultant : {this.state.consultantById.nom}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteConsultant.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      </div>

    );
  }
}  
export default consultant;