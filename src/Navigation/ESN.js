import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'

class ESN extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      esns:[],
      esnById:{},
      index:null,
      values: [],
      query: "",
      data: [],
      users:[],
      dv : [],
      sam : ''
    };
  }

  handleChange(i, e) {
    if (e) {
      if (e) {
        switch (i) {
          case 'societe': {
            this.state.esnById.societe = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;    
          case 'tel_fixe': {
            this.state.esnById.tel_fixe = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'mobile': {
            this.state.esnById.mobile = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'poste': {
            this.state.esnById.poste = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'email': {
            this.state.esnById.email = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'interlocuteur': {
            this.state.esnById.interlocuteur = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break;
          case 'commentaire': {
            this.state.esnById.commentaire = e.target.value;
            this.setState({ esnById: this.state.esnById });
          }
          break; 
          case (i): {
            let values = [...this.state.values];
              values[i] = e.target.value;
              this.setState({  values  });             
          }       
          break;  
          default: 
          break;
        }   
      }
    }
  }

  componentDidMount() {    
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        for(var i=0; i<data.length; i++){
          this.setState({users:data})   
        }
      })   
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/AllEsns")
      .then(response => response.data)
      .then((data) => {
        this.setState({ esns: data });  
      });
  }

  componentWillMount() {
    this.componentDidMount();
  }

  AddEsn(){
    this.state.esnById.societe = '';
    this.state.esnById.interlocuteur = '';
    this.state.esnById.tel_fixe = '';
    this.state.esnById.mobile = '';
    this.state.esnById.poste = '';
    this.state.esnById.email = '';
    this.state.esnById.commentaire = '';
    this.setState({ esnById: this.state.esnById })
  } 

  // ajouter une SSII
  ValidateEsn() { 
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/aEsn", this.state.esnById) 
      .then(response => response.data)
      .then((esnById) => {
        this.state.esns.push(esnById);       
        this.setState({users:this.state.esns});
      }).catch((error) => { console.log(error) 
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        un probléme detetcté lors de l'insertion de votre nouvelle Esn</p>
        this.setState({message:this.state.message})
      });
  }

  //supprimer une SSII
  DeleteEsn(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/deleteEsn/"+this.state.esnById.idesn)
      .then(response => response.data)
      .then((data) => {
        this.state.esns.splice(this.state.index,1);
        this.setState({users:this.state.esns});
      }).catch((error) => { console.log(error) });
  }

  //lire une SSII ( pas de tout les SSII)
  handleSort(idesn,index) {
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/esn/" + idesn)
      .then(response => response.data)
      .then((data) => {
        this.setState({ esnById: data });
        // this.state.esnById.interlocuteur = this.state.esnById.interlocuteur.split(' - ')
        this.state.index=index ;            
      });
  }

  //modifier une SSII
  UpdateEsn(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/updateEsn",this.state.esnById)
      .then(response => response.data)
      .then((data) => {
        this.state.esns.splice(this.state.index,1);
        this.state.esns.push(data);
        this.setState({esns:this.state.esns});
       // window.location.reload(false);
      }).catch((error) => { console.log(error)
        this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
        votre modification est echouée</p>
        this.setState({message:this.state.message}) 
      });
  }

  render() {
    return (
      <div class="backtotal">
       <NavigationBar/>
       
        <div className="container">
            
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddEsn()}>
              <i className="fas fa-plus-circle"></i> Ajouter une nouvelle ESN
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
          <table className="table table-sm" >
            <thead>
              <tr>
                <th scope="col" hidden={true}></th>
                <th scope="col"><i class="fas fa-industry"></i> Nom</th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Telephone Fixe </th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Portable </th>
                <th scope="col"><i class="fas fa-envelope"></i> Email</th>
                <th scope="col"><i class="fas fa-user"></i> Interlocuteur</th>
                <th scope="col"><i class="fas fa-briefcase"></i> Poste</th>
                <th scope="col"><i class="fas fa-comments"></i> Commentaire</th>
                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody> 
             {this.state.esns.map((esn,index) => (
                <tr >
                  <td hidden={true}>{esn.idesn}</td>
                  <td>{esn.societe} </td>
                  <td>{esn.tel_fixe}</td>
                  <td>{esn.mobile}</td>
                  <td>{esn.email}</td>
                  <td>{esn.interlocuteur}</td> 
                  <td>{esn.poste}</td>
                  <td> {esn.commentaire.length>=20? 
                          <div key={esn.commentaire.length}>{esn.commentaire.substring(0, 20) + ' ...'}</div> 
                        : <div>{esn.commentaire} </div>
                       }
                  </td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(esn.idesn,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données de l'ESN: {this.state.esnById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-industry"></i> Société</td>
                      <td>{this.state.esnById.societe}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Telephone Fixe </td>
                      <td>{this.state.esnById.tel_fixe}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Portable </td>
                      <td>{this.state.esnById.mobile}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-envelope"></i> Email </td>
                      <td>{this.state.esnById.email}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-user"></i> Interlocuteur </td>
                     <td>{this.state.esnById.interlocuteur}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-briefcase"></i> Poste </td>
                      <td>{this.state.esnById.poste}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-comments"></i> Commentaire</td>
                      <td>{this.state.esnById.commentaire}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.esnById.societe} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input required type="text" class="form-control" value={this.state.esnById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le nom de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone Fixe</label> 
                    <input type="text" class="form-control" value={this.state.esnById.tel_fixe} onChange={this.handleChange.bind(this, "tel_fixe")} placeholder="insérez le telephone fixe de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Portable</label> 
                    <input type="text" class="form-control" value={this.state.esnById.mobile} onChange={this.handleChange.bind(this, "mobile")} placeholder="insérez le telephone portable de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.esnById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez l'email de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                    <input  type="text" class="form-control" value={this.state.esnById.interlocuteur}
                     onChange={this.handleChange.bind(this, "interlocuteur")} 
                     placeholder="insérez le nom de l'interlocuteur"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    <input type="text" class="form-control" value={this.state.esnById.poste} onChange={this.handleChange.bind(this, "poste")} placeholder="insérez le poste de l'interlocuteur"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input required type="text" class="form-control" value={this.state.esnById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateEsn.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter une ESN </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Société</label>
                    <input type="text" class="form-control" value={this.state.esnById.societe} onChange={this.handleChange.bind(this, "societe")} placeholder="insérez le nom de l'ESN"></input>
                  </div>
                 
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone Fixe</label> 
                    <input type="text" class="form-control" value={this.state.esnById.tel_fixe} onChange={this.handleChange.bind(this, "tel_fixe")} placeholder="insérez le telephone fixe de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Portable</label> 
                    <input type="text" class="form-control" value={this.state.esnById.mobile} onChange={this.handleChange.bind(this, "mobile")} placeholder="insérez le telephone portable de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.esnById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez l'email de l'ESN"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Interlocuteur</label>
                    <input  type="text" class="form-control" value={this.state.esnById.interlocuteur}
                     onChange={this.handleChange.bind(this, "interlocuteur")} 
                     placeholder="insérez le nom de l'interlocuteur"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Poste</label>
                    <input type="text" class="form-control" value={this.state.esnById.poste} onChange={this.handleChange.bind(this, "poste")} placeholder="insérez le poste de l'interlocuteur"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input type="text" class="form-control" value={this.state.esnById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateEsn.bind(this)} data-dismiss="modal">Ajouter l'ESN</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion ESN</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer l'ESN : {this.state.esnById.societe}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteEsn.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      </div>

    );
  }
}  
export default ESN;