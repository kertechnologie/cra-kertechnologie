import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'

class candidat extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        candidats:[],
        candidatById:{},
        index:null,
        style:"",
        query: "",
        data: [],
        filteredData: []
      };
    }

    handleChange(label, e) {
        if (e) {
          if (e) {
            switch (label) {
              case 'specialite': {
                this.state.candidatById.specialite = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'nom': {
                this.state.candidatById.nom = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'prenom': {
                this.state.candidatById.prenom = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'tel': {
                this.state.candidatById.tel = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'poste': {
                this.state.candidatById.poste = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'email': {
                this.state.candidatById.email = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'specialite': {
                this.state.candidatById.specialite = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'statut': {
                this.state.candidatById.statut = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                break;
              case 'commentaire': {
                this.state.candidatById.commentaire = e.target.value;
                this.setState({ candidatById: this.state.candidatById });
              }
                    break;
              default: 
              break;
      
          }
    
        }
      }
    }

    componentDidMount() {       
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.get("http://45.9.188.225:8080/cra/AllCandidats")
        .then(response => response.data)
        .then((data) => {
          const { query } = this.state;
        const filteredData = data.filter(element => {
          return ((element.nom.toLowerCase().includes(query.toLowerCase())) 
              ||(element.prenom.toLowerCase().includes(query.toLowerCase()))
              ||(element.statut.toLowerCase().includes(query.toLowerCase()))) ;
        });
        this.setState({ data, filteredData});
          this.setState({ candidats: data });
        });
    }

    handleInputChange = event => {
      const query = event.target.value;
      this.setState(prevState => {
        const filteredData = prevState.data.filter(element => {
          return ((element.nom.toLowerCase().includes(query.toLowerCase())) 
                ||(element.prenom.toLowerCase().includes(query.toLowerCase()))
                ||(element.statut.toLowerCase().includes(query.toLowerCase()))) ;
                //.toLowerCase().includes(event.target.value.toLowerCase())
          }); 
          console.log("bb",filteredData.poste)
  
          return { query, filteredData };  
      });
    };

    AddCandidat(){
      this.state.candidatById.nom = '';
      this.state.candidatById.prenom = '';
      this.state.candidatById.tel = '';
      this.state.candidatById.poste = '';
      this.state.candidatById.email = '';
      this.state.candidatById.specialite = '';
      this.state.candidatById.statut = '';
      this.state.candidatById.commentaire = '';
      this.setState({ candidatById: this.state.candidatById })
    }

    ValidateCandidat() { 
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.post("http://45.9.188.225:8080/cra/aCandidat", this.state.candidatById)    
        .then(response => response.data)
        .then((candidatById) => {
          this.state.candidats.push(candidatById);
          console.log(candidatById)
          this.setState({users:this.state.candidats});
          console.log(this.state.users)
        }).catch((error) => { console.log(error) 
          this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
          un probléme detetcté lors de l'insertion de votre nouveau candidat</p>
          this.setState({message:this.state.message})
        });
    }

    DeleteCandidat(){
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.get("http://45.9.188.225:8080/cra/deleteCandidat/"+this.state.candidatById.idcandidat)
        .then(response => response.data)
        .then((data) => {
         this.state.candidats.splice(this.state.index,1);
         this.setState({users:this.state.candidats});
         }).catch((error) => { console.log(error) });
   }

    handleSort(idcandidat,index) {
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.get("http://45.9.188.225:8080/cra/candidat/" + idcandidat)
        .then(response => response.data)
        .then((data) => {
          this.setState({ candidatById: data });
          this.state.index=index ;            
        });
    }

    UpdateCandidat(){
      axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
      axios.post("http://45.9.188.225:8080/cra/updateCandidat",this.state.candidatById)
        .then(response => response.data)
        .then((data) => {
         this.state.candidats.splice(this.state.index,1);
         this.state.candidats.push(data);
         this.setState({candidats:this.state.candidats});
        }).catch((error) => { console.log(error)
          this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
          votre modification est echouée</p>
          this.setState({message:this.state.message}) 
       });
   }

   render() {
    return (
      <div class="backtotal">
       <NavigationBar/>
        <div className="container">
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddCandidat()}>
              <i className="fas fa-plus-circle"></i> Ajouter un nouveau Candidat
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
          <div className="searchForm rr">
            <form>
            <div className="soci"><label>Rechercher par Nom, Prénom ou par Statut :</label> 
             <input class="tstbor" placeholder="Search for..." value={this.state.query} onChange={this.handleInputChange} />
            </div>
            </form>
          </div>
          <table className="table table-sm">
            <thead>
              <tr>
                <th scope="col" hidden={true}></th>
                <th scope="col"><i class="fas fa-user"></i> Nom</th>
                <th scope="col"><i class="fas fa-user"></i> Prénom</th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Telephone </th>
                <th scope="col"><i class="fas fa-briefcase"></i> Profil</th>
                <th scope="col"><i class="fas fa-envelope"></i> Email</th>
                <th scope="col"><i class="fas fa-address-card"></i> Compétence</th>
                <th scope="col"><i class="fas fa-address-card"></i> Statut</th>
                <th scope="col"><i class="fas fa-comments"></i> Commentaire</th>
                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.filteredData.map((candidat,index) => (
                <tr >
                  <td hidden={true}> {candidat.idcandidat}</td>
                  <td>{candidat.nom}</td>
                  <td>{candidat.prenom}</td>
                  <td>{candidat.tel}</td>
                  <td>{candidat.poste}</td>
                  <td>{candidat.email}</td>
                  <td>{candidat.specialite}</td>
                  <td>{candidat.statut}</td>
                  <td>{candidat.commentaire?.length>=20? 
                          <div key={candidat.commentaire.length}>{candidat.commentaire.substring(0, 20) + ' ...'}</div> 
                        : <div>{candidat.commentaire} </div>}</td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(candidat.idcandidat,index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(candidat.idcandidat,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(candidat.idcandidat,index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(candidat.idcandidat,index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données du candidat: {this.state.candidatById.prenom} {this.state.candidatById.nom} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-user"></i> Nom </td>
                      <td>{this.state.candidatById.nom} </td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-envelope"></i> Prénom </td>
                      <td>{this.state.candidatById.prenom}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Telephone </td>
                      <td>{this.state.candidatById.tel}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Profil </td>
                      <td>{this.state.candidatById.poste}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Compétence</td>
                      <td>{this.state.candidatById.specialite}</td>
                    </tr>
                      <td><i class="fas fa-calendar-alt"></i> Email </td>
                      <td>{this.state.candidatById.email}</td>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Statut</td>
                      <td>{this.state.candidatById.statut}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Commentaire</td>
                      <td>{this.state.candidatById.commentaire}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.candidatById.prenom} {this.state.candidatById.nom} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Nom</label>
                    <input required type="text" class="form-control" value={this.state.candidatById.nom} onChange={this.handleChange.bind(this, "nom")} placeholder="insérez le nom du candidat"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Prénom</label>
                    <input required type="text" class="form-control" value={this.state.candidatById.prenom} onChange={this.handleChange.bind(this, "prenom")} placeholder="insérez le prénom du candidat"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label>
                    <input required type="text" class="form-control" value={this.state.candidatById.tel} onChange={this.handleChange.bind(this, "tel")} placeholder="insérez le telephone du candidat"></input>
                  </div>
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Profil</label>
                    <select class="form-control" value={this.state.candidatById.poste}  onChange={this.handleChange.bind(this, "poste")}>
                      <option value=""></option>
                      <option value="Full stack-Java">Full stack-Java</option>
                      <option value="Full stack-.Net">Full stack-.Net</option>
                      <option value="Full stack-PHP">Full stack-PHP</option>
                      <option value="DEVOPS">DEVOPS</option>
                      <option value="Talend">Talend</option>
                      <option value="MSBI">MSBI</option>
                      <option value="Informatica">Informatica</option>
                      <option value="DataStage">DataStage</option>
                      <option value="BI">BI</option>
                      <option value="MOA">MOA</option>
                      <option value="AMOA">AMOA</option>
                      <option value="Salesforce">Salesforce</option>
                      <option value="Système">Système</option>
                      <option value="Réseau">Réseau</option>
                      <option value="Sécurité">Sécurité</option>
                      <option value="DBA">DBA</option>
                      <option value="CRM">CRM</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Compétence</label>
                    <input type="text" class="form-control" value={this.state.candidatById.specialite} onChange={this.handleChange.bind(this, "specialite")} placeholder="insérez les compétences du candidat"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.candidatById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez l'email du candidat"></input>
                  </div>
                  
                    <label for="exampleFormControlSelect1">Statut</label>
                    <select className="form-control" value={this.state.candidatById.statut} onChange={this.handleChange.bind(this, "statut")}>
                      <option value=""></option>
                      <option value="Recruté">Recruté</option>
                      <option value="En cours">En cours</option>
                      <option value="Refusé">Refsué</option>
                    </select>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input required type="text" class="form-control" value={this.state.candidatById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateCandidat.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter un candidat </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Nom</label>
                    <input type="text" class="form-control" value={this.state.candidatById.nom} onChange={this.handleChange.bind(this, "nom")} placeholder="insérez le nom du candidat"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Prénom</label>
                    <input type="text" class="form-control" value={this.state.candidatById.prenom} onChange={this.handleChange.bind(this, "prenom")} placeholder="insérez le prénom du candidat"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label>
                    <input type="text" class="form-control" value={this.state.candidatById.tel} onChange={this.handleChange.bind(this, "tel")} placeholder="insérez le telephone du candidat"></input>
                  </div>
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Profil</label>
                    <select class="form-control" value={this.state.candidatById.poste}  onChange={this.handleChange.bind(this, "poste")}>
                      <option value=""></option>
                      <option value="Full stack-Java">Full stack-Java</option>
                      <option value="Full stack-.Net">Full stack-.Net</option>
                      <option value="Full stack-PHP">Full stack-PHP</option>
                      <option value="DEVOPS">DEVOPS</option>
                      <option value="Talend">Talend</option>
                      <option value="MSBI">MSBI</option>
                      <option value="Informatica">Informatica</option>
                      <option value="DataStage">DataStage</option>
                      <option value="BI">BI</option>
                      <option value="MOA">MOA</option>
                      <option value="AMOA">AMOA</option>
                      <option value="Salesforce">Salesforce</option>
                      <option value="Système">Système</option>
                      <option value="Réseau">Réseau</option>
                      <option value="Sécurité">Sécurité</option>
                      <option value="DBA">DBA</option>
                      <option value="CRM">CRM</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Compétence</label>
                    <input type="text" class="form-control" value={this.state.candidatById.specialite} onChange={this.handleChange.bind(this, "specialite")} placeholder="insérez les compétences du candidat"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.candidatById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez l'email du candidat"></input>
                  </div>
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Statut</label>
                    {/* <input type="text" class="form-control" value={this.state.up_conges.motif}  onChange={this.handleChange.bind(this, "motif")} placeholder="insérez le motif de l'absence"></input> */}
                    <select class="form-control" value={this.state.candidatById.statut}  onChange={this.handleChange.bind(this, "statut")}>
                      <option value=""></option>
                      <option value="Recruté">Recruté</option>
                      <option value="En cours">En cours</option>
                      <option value="Refusé">Refsué</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Commentaire</label>
                    <input type="text" class="form-control" value={this.state.candidatById.commentaire} onChange={this.handleChange.bind(this, "commentaire")} placeholder="insérez un commentaire"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateCandidat.bind(this)} data-dismiss="modal">Ajouter le candidat</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion candidat</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer le candidat : {this.state.candidatById.prenom} {this.state.candidatById.nom}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteCandidat.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      </div>

    );
  }
}  
export default candidat;