import React from 'react';
import '../App.css';
import NavigationBar from './NavigationVar';

import { Redirect } from 'react-router-dom';


import { parseJwt } from '../util/JWParser';


class profil extends React.Component {

  constructor(props) {

    super(props);
    const token = sessionStorage.getItem("token");
    let login = true;
    if (token == null) {
      login = false
    }
    this.state = {
      users: [],
      login,
      token: token
    };
  }

  componentDidMount() {
    var decoded = parseJwt(sessionStorage.getItem("token"));
    if (decoded.exp < new Date().getTime() / 1000) {
      this.setState({ login: false });
    } else {
      this.setState({ users: decoded });
    }

  }

  render() {

    if (this.state.login === false) {
      return <Redirect to="/" />
    }
    return (
      <div class="backtotal">
        <NavigationBar />
        <div className="container">


          <div className="row profilrow">
            <div className="col-1"></div>
            <div class="col-2 ">
              {/* <center><h2>Mon<br></br>Profil</h2></center> */}
              </div>
              <div class="col-7">
              <div className="row btn btn-warning btn-block disabled"><b>Mon Profil</b></div> 
          <table className="table table-sm profil">
                  
                  <tbody>
                    <tr>
                      <td><i class="fas fa-address-card"></i> Pseudo</td>
                      <td>{this.state.users.pseudo}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-user"></i> Nom </td>
                      <td>{this.state.users.prenom} {this.state.users.nom}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-envelope"></i> Email</td>
                      <td>{this.state.users.email}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Adresse</td>
                      <td>{this.state.users.adresse}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Telephone</td>
                      <td>{this.state.users.telephone}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Date de naissance</td>
                      <td>{this.state.users.date_naissance}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Date Embauche</td>
                      <td>{this.state.users.date_embauche}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-file-signature"></i> Contrat</td>
                      <td>{this.state.users.contrat}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-heartbeat"></i> Numéro Securité Sociale</td>
                      <td>{this.state.users.num_secu}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-user-tag"></i> Role</td>
                      <td>{this.state.users.role}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-euro-sign"></i> Salaire</td>
                      <td>{this.state.users.salaire}</td>
                    </tr>
                  </tbody>
                </table>
                </div>
        </div>
        </div>
      </div>);
  }
}

export default profil;