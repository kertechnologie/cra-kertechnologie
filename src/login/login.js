import React from "react"; 
import axios from 'axios';
import loginImg from "../kertec.jpg";
import { Redirect } from 'react-router-dom';
import {parseJwt} from '../util/JWParser' ;
//import App from "../App";

 class login extends React.Component {
    constructor(propos)
    {               

        super(propos);
        const token =sessionStorage.getItem("token");
        let login = true;
        if (token == null){
            login=false
        }
        this.state=
        {value: '',
        password:'',
        log : false,
        login,
        role : '',
        decoded:''
        }

    this.handleChange = this.handleChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
        
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
     }

  
    handleSubmit(event) {
        event.preventDefault();
        var x = "";
        axios.post("http://45.9.188.225:8080/cra/login",{pseudo: this.state.value,pwd: this.state.password})
          .then(response=> response.data)
          .then((data) =>{
             this.state.decoded = parseJwt(data.token) ;
            //console.log("decoded",decoded.role);
              sessionStorage.setItem("token",data.token); 
              sessionStorage.setItem("role",this.state.decoded);    
              axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
              this.setState({log:true})
            //  this.state.role=decoded.role
            //console.log(this.state.role)
          }).catch((error) => { console.log(error)
            this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
            Vos données de connexion sont erronées</p>
            this.setState({message:this.state.message}) })
      }

    render(){
        if(this.state.log){
            if(sessionStorage.getItem("redirectTo")){
                var redirect = sessionStorage.getItem("redirectTo") ;
                // sessionStorage.removeItem("redirectTo") ;          
                switch(redirect) {
                    case "profil" :  return <Redirect to ='/profil'/>
                    case "calendrier" : return <Redirect to = '/calendrier'/> 
                 //   default : return <Redirect to = '/calendrier'/> ;
                }
              
            } else {
                if((this.state.decoded.role=="Utilisateur")||(this.state.decoded.role=="Admin")){
                    return <Redirect to = '/calendrier'/> ;
                }else{
                    return <Redirect to = '/profil'/> ;
                }
            }
        }
       
        return(
            <div>
                <nav className="navbar " width="100%"></nav>
                <div className="container" ref={this.props.containerRef}>
                    <div className="row" class="bkrgnd">
                    
                        <div className="col-lg-12">
                           
                            <div className="logform"> 
                                {this.state.message != "" ? this.state.message : ""}
                                <form className="form" onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="pseudo" className="lab lgn">Pseudo</label>
                                        <input type="text" className="form-control" name="pseudo" value={this.state.value} onChange={this.handleChange} placeholder="votre pseudo"></input>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="pseudo" class="lgn">Mot de passe</label>
                                        <input type="password" name="pwd" className="form-control" value={this.state.pwd} onChange={this.handlePasswordChange} placeholder="votre mot de passe"></input>
                                    </div>
                                    <button type="submit" className="btn btn-primary btn-block" id="submit">Se connecter</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    }

export default login;