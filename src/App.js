import React, { Component } from 'react';

import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// import TodoList from'./Todo/TodoList'
// import NavigationBar from './Navigation/NavigationVar'
import Calendrier from './Navigation/Calendrier';
import profil from './Navigation/profil';
import login from './login/login';
import ListCra from './Navigation/ListCra';
import ListeProjet from './Navigation/ListeProjet';
import chart from './Navigation/conges';
import checkUserTask from './Navigation/CheckUserTasks'
import conges from './Navigation/conges';
import gestion_conges from './Navigation/gestion_conges';
import client from './Navigation/client';
import candidat from './Navigation/candidat';
import prospect from './Navigation/prospect';
import facture from './Navigation/facture';
import ESN from './Navigation/ESN';
import consultant from './Navigation/consultant';
import opportunite from './Navigation/opportunite';
import change_password from './Navigation/change_password';

class App extends Component {


  render() {
    return (
      <div className="App" >

        <BrowserRouter basename='/'>
          <Switch>
            <Route path="/" exact component={login} />
            <Route path="/calendrier" exact component={Calendrier} />
            <Route path="/profil" exact component={profil} />
            <Route path="/users" exact component={ListCra} />
            <Route path="/projet" exact component={ListeProjet} />
            <Route path="/conges" exact component={conges} />
            <Route path="/cra" exact component={checkUserTask} />
            <Route path="/client" exact component={client} />
            <Route path="/prospect" exact component={prospect} />
            <Route path="/facture" exact component={facture} />
            <Route path="/ESN" exact component={ESN} />
            <Route path="/consultant" exact component={consultant} />
            <Route path="/opportunite" exact component={opportunite} />
            <Route path="/candidat" exact component={candidat} />
            <Route path="/gestion_conges" exact component={gestion_conges}/>
            <Route path="/pwd" exact component={change_password}/>
            
          </Switch>
        </BrowserRouter>

      </div>
    );
  }
}
export default App;
